﻿Imports System.IO
Imports System.Text

Public Class ComFunction
    ' ログ出力
    Public Sub LogOut(ByVal msg As String, Optional ByVal flg As Boolean = False,
                    <System.Runtime.CompilerServices.CallerMemberName> Optional callerMemberName As String = Nothing,
                    <System.Runtime.CompilerServices.CallerFilePath> Optional callerFilePath As String = Nothing,
                    <System.Runtime.CompilerServices.CallerLineNumber> Optional callerLineNumber As Integer = 0)
        Dim Message As String

        '呼び出し元メソッド 呼び出し元行番号  メッセージ
        Message = callerMemberName & "[" & callerLineNumber & "]" & "「" & msg & "」"
        Console.WriteLine(Message)  'Console出力
        If flg = True Then
            WriteLog(Message)           'ファイル出力
        End If

        If (callerMemberName = ".ctor" OrElse        ' コンストラクタから呼ばれた場合
               callerMemberName = ".cctor" OrElse    ' 静的コンストラクタから呼ばれた場合
               callerMemberName = "Finalize") Then   ' デストラクタから呼ばれた場合
            Throw New Exception("コンストラクタで呼ぶなって言ってんだろ。このバカチンが！")
        End If
    End Sub

    Public Sub WriteLog(ByVal msg As String, Optional ByVal ex As Exception = Nothing)
        Try
            ' ログフォルダ名作成
            Dim dt As DateTime = Now
            Dim logFolder As String = System.Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) & "\BlueMarlin"

            ' ログフォルダ名作成
            System.IO.Directory.CreateDirectory(logFolder)

            ' ログファイル名作成
            Dim logFile As String = logFolder & "\TraceLog_" & dt.DayOfWeek & ".log"

            ' 翌日分のログファイル削除(１週間分のログファイルしか保存しないようにするため)
            Dim logNext As String =
                logFolder & "\TraceLog_" & dt.AddDays(1).DayOfWeek & ".log"
            System.IO.File.Delete(logNext)

            ' ログ出力文字列作成
            Dim logStr As String
            logStr = dt.ToString("yyyy/MM/dd HH:mm:ss") & vbTab & msg
            If ex Is Nothing = False Then
                logStr = logStr & vbCrLf & ex.ToString
            End If

            ' Shift-JISでログ出力
            Dim sw As IO.StreamWriter = Nothing
            Try
                sw = New IO.StreamWriter(logFile, True,
                    System.Text.Encoding.GetEncoding("Shift-JIS"))
                sw.WriteLine(logStr)
            Catch ex2 As Exception
            Finally
                If sw Is Nothing = False Then sw.Close()
            End Try
        Catch ex2 As Exception
        End Try
    End Sub
End Class
