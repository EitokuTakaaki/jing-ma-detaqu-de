﻿Imports System.IO
Imports System.Net
Imports System.Text
Imports MySql.Data.MySqlClient
Imports Renci.SshNet

Public Class HorseRacingTips
    Private JVInitFlg As Integer   'JVInit定義フラグ
    Private lDownloadCount As Long ''JVOpen:総ダウロードファイル数
    Private JVOpenFlg As Boolean ''JVOpen 状態フラグ Opne 時:Ture
    Private strFromTime As String '' 引数 JVOpen:データ提供日付
    Private strLastFileTimestamp As String '' JVOpen: 最新ファイルのタイムスタンプﾟ
    Private strLastFileMstTimestamp As String '' JVOpen: 最新ファイルのタイムスタンプﾟ
    Private objCodeConv As Object ''コード変換インスタンス
    Private DB As MySQLComCls  '' MySQL制御クラス
    Private lStopFlag As Long = 0      '取得終了フラグ
    Private DbData As Db_Connet_Data  ''データベース接続情報
    Private connectionString As String   ''データベース接続情報
    Private TimerInterval As Long   'データ取得インターバル（１０分）
    Private DBInitFlg As Integer   'DB定義フラグ
    Private COM As ComFunction     '共通関数
    Private MstGetFlag As Boolean  '
    Private tmpToday As String     '
    Private RacesList() As String   'レースリスト（リアルタイムオッズ取得で使う）

    Private Sub HorseRacingTips_Load(sender As Object, e As EventArgs) Handles Me.Load
        '***************
        'JVLink初期化
        '***************
        Dim ReturnCode As Integer
        Dim sid As String
        'INIファイルからの取得
        Dim str As String

        COM = New ComFunction
        '引数設定
        sid = "UNKNOWN"
        'バージョン表示
        Dim ver As System.Diagnostics.FileVersionInfo
        ver = System.Diagnostics.FileVersionInfo.GetVersionInfo(System.Reflection.Assembly.GetExecutingAssembly().Location)
        lblVersion.Text = "[Ver." & ver.FileVersion & "]"

        'INIフォルダ
        Dim tempPath = Environment.GetEnvironmentVariable("temp")
        Dim strPath = tempPath & "\..\BlueMarlin"
        If System.IO.Directory.Exists(strPath) = False Then
            System.IO.Directory.CreateDirectory(strPath)
        End If

        JVInitFlg = DBInitFlg = 0
        'デフォルトのDB情報を設定
        DbData.DbName = "HorseRacingTipsDB"
        DbData.DbUser = "HRT"
        DbData.DbPassword = "HorseRacingTips"
        DbData.DbServer = "139.162.88.225"

        'ファイル存在チェック
        Dim boolFile_Exists As Boolean
        boolFile_Exists = System.IO.File.Exists(strPath & "\horseracing.ini")
        If boolFile_Exists = True Then
            'INIファイルクラスの生成
            Dim Ini As New clsIniConfig(strPath & "\horseracing.ini")
            str = Ini.GetProfileString("JVINIT", "SETTING", "OFF")
            COM.LogOut("JVINIT:SETTING=" & str, True)
            If str = "ON" Then
                JVInitFlg = 0
            Else
                JVInitFlg = -1
            End If
            str = Ini.GetProfileString("DATABASE", "SERVER_NAME", "NOTHING")
            COM.LogOut("DATABASE:SERVER_NAME=" & str, True)
            If str = "NOTHING" Then
                DBInitFlg = -1
            Else
                DbData.DbServer = str
            End If
            str = Ini.GetProfileString("DATABASE", "DATABASE_NAME", "NOTHING")
            COM.LogOut("DATABASE:DATABASE_NAME=" & str, True)
            If str = "NOTHING" Then
                DBInitFlg = -1
            Else
                DbData.DbName = str
            End If
            str = Ini.GetProfileString("DATABASE", "DATABASE_USER", "NOTHING")
            COM.LogOut("DATABASE:DATABASE_USER=" & str, True)
            If str = "NOTHING" Then
                DBInitFlg = -1
            Else
                DbData.DbUser = str
            End If
            str = Ini.GetProfileString("DATABASE", "DATABASE_PASSWORD", "NOTHING")
            COM.LogOut("DATABASE:DATABASE_PASSWORD=" & str, False)
            If str = "NOTHING" Then
                DBInitFlg = -1
            Else
                DbData.DbPassword = str
            End If
        End If

        'MySQL制御インスタンス
        DB = New MySQLComCls
        'Timerの間隔を1000msec(1sec)に設定
        Me.GetTimer.Interval = 1000
        'Timerを非活性にする
        Me.GetTimer.Enabled = False

        connectionString = "server=" & DbData.DbServer & ";database=" & DbData.DbName & ";user id=" & DbData.DbUser & ";password=" & DbData.DbPassword
        'COM.LogOut(connectionString, False)
        'データ取得インターバル
        TimerInterval = 1000 * 60 * 10  '１０分
        lblCountDown.Text = "取得待機"

        'COM.LogOut("JVInitFlg:" & JVInitFlg, False)
        If JVInitFlg <> 0 Then
            AllButtonControl(False)
            btnJVInitSetting.Enabled = True
            ProgramEnd.Enabled = True
        End If
        'JVLink 初期化
        ReturnCode = Me.AxJVLink1.JVInit(sid)
        'COM.LogOut("JVInit:" & ReturnCode, False)
        'エラー判定
        If ReturnCode <> 0 Then ''エラー
            MsgBox("JVInitエラー:" & ReturnCode)
        Else ''正常
            ' イベント監視オブジェクト初期化
            ReturnCode = AxJVLink1.JVWatchEvent()
            'COM.LogOut("JVWatchEvent:" & ReturnCode, False)
            If ReturnCode <> 0 Then ''エラー
                MsgBox("JVWatchEventエラー" & ReturnCode)
            End If
        End If

        'コード変換インスタンス生成
        objCodeConv = New clsCodeConv
        'パスを指定し、コードファイルを読込む
        objCodeConv.FileName = Application.StartupPath & "\CodeTable.csv"
        COM.LogOut("CovFile:" & Application.StartupPath & "\CodeTable.csv", True)
        Dim DefaultNow As DateTime = DateTime.Now
        Dim DefaultGetDate As DateTime
        'DefaultGetDate = DefaultNow.AddYears(-5)
        DefaultGetDate = DefaultNow.AddDays(-8)
        strFromTime = Format(DefaultGetDate, "yyyyMMdd") & "000000"
        'マスタデータ取得は今のところ非活性
        'JVDataGetMst.Enabled = False
        'COM.LogOut(strPath, False)
    End Sub

    Private Sub HorseRacingTips_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        ' イベント監視オブジェクト終了
        Dim ReturnCode As Integer = AxJVLink1.JVWatchEventClose()
    End Sub

    Private Sub btnJVInitSetting_Click(sender As Object, e As EventArgs) Handles btnJVInitSetting.Click
        Dim lReadCount As Long '' JVLink 戻り値
        Dim lDownloadCount As Long '' JVOpen:総ダウンロードファイル数
        Dim strLastTimestamp As String '' JVOpen: 最新ファイルのタイムスタンプﾟ
        Dim lReturnCode As Long ' リターンコード
        Try
            strLastTimestamp = ""
            ' 設定画面表示
            lReturnCode = AxJVLink1.JVSetUIProperties()
            If lReturnCode <> 0 Then
                MsgBox("JVSetUIPropertiesエラー コード：" & lReturnCode & "：", MessageBoxIcon.Error)
            Else
                lReturnCode = Me.AxJVLink1.JVOpen("COMM", strFromTime, 1, lReadCount, lDownloadCount, strLastTimestamp)
                If lReturnCode = 0 Or lReturnCode = -1 Then
                    'INIフォルダ
                    Dim tempPath = Environment.GetEnvironmentVariable("temp")
                    Dim strPath = tempPath & "\..\BlueMarlin"
                    'ファイル存在チェック
                    Dim boolFile_Exists As Boolean
                    boolFile_Exists = System.IO.File.Exists(strPath & "\horseracing.ini")
                    If boolFile_Exists = False Then
                        ' ファイルに対するリファレンスを作成する
                        Dim fi As FileInfo = New FileInfo(strPath & "\horseracing.ini")
                        ' 実際にファイルを作成する
                        Using fs As FileStream = fi.Create()
                            AddText(fs, "[JVINIT]" & vbCrLf)
                            AddText(fs, "SETTING=" & "ON")
                            fs.Close()
                        End Using
                    Else
                        'INIファイルクラスの生成
                        Dim Ini As New clsIniConfig(strPath & "\horseracing.ini")
                        'INIファイルへの書込み
                        Ini.WriteProfileString("JVINIT", "SETTING", "ON")
                    End If
                    JVInitFlg = 0
                    AllButtonControl(True)
                End If

            End If
        Catch ex As Exception
            MsgBox("JVSetUIProperties例外エラー コード：" & ex.Message)
        End Try
    End Sub
    Private Sub JVDataGetStop_Click(sender As Object, e As EventArgs) Handles JVDataGetStop.Click
        Me.GetTimer.Stop()
        StatusLabel.Text = String.Format("取得終了：イベント入力待ち")
    End Sub

    Private Sub btnDBSetting_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDBSetting.Click

        Dim wkSubForm As New frmDBSet()
        '入力パラメータの設定
        wkSubForm.txtDBName.Text = DbData.DbName
        wkSubForm.txtDBUser.Text = DbData.DbUser
        wkSubForm.txtDBPassword.Text = DbData.DbPassword
        wkSubForm.txtDBServer.Text = DbData.DbServer

        If System.Windows.Forms.DialogResult.OK = wkSubForm.ShowDialog() Then
            DbData.DbName = wkSubForm.txtDBName.Text
            DbData.DbUser = wkSubForm.txtDBUser.Text
            DbData.DbPassword = wkSubForm.txtDBPassword.Text
            DbData.DbServer = wkSubForm.txtDBServer.Text
            COM.LogOut("DATABASE:SERVER_NAME=" & DbData.DbServer, True)
            COM.LogOut("DATABASE:DATABASE_NAME=" & DbData.DbName, True)
            COM.LogOut("DATABASE:DATABASE_USER=" & DbData.DbUser, True)
            COM.LogOut("DATABASE:DATABASE_PASSWORD=" & DbData.DbPassword, True)

        End If
        connectionString = "server=" & DbData.DbServer & ";database=" & DbData.DbName & ";user id=" & DbData.DbUser & ";password=" & DbData.DbPassword
        'COM.LogOut(connectionString, False)
    End Sub

    Private Sub JVDataGetMst_Click(sender As Object, e As EventArgs) Handles JVDataGetMst.Click
        Dim Rtn As Long
        AllButtonControl(False)
        Rtn = GetScheduleData()
        AllButtonControl(True)
    End Sub

    Private Sub JVDataGetPast_Click(sender As Object, e As EventArgs) Handles JVDataGetPast.Click
        Dim intRet As Integer
        Dim Rtn As Long
        Dim inputText As String
        Dim FromTimestamp As String
        inputText = InputBox("何年以降のデータを取得しますか？", "取得年", "2017", 200, 100)
        If inputText <> vbNullString Then
            intRet = MsgBox("過去データ（レースデータ、馬、騎手）を取得します。" & vbCrLf _
                        & inputText & "年以降のデータを取得するので時間がかかります。" & vbCrLf _
                        & "レース情報の取得タイマが動いていたら停止して下さい。" & vbCrLf _
                        & "よろしいですか？" & vbCrLf _
                        , MsgBoxStyle.YesNo, "確認")
            If intRet = MsgBoxResult.Yes Then
                lblCountDown.Text = "マスタデータ取得中！"
                AllButtonControl(False)
                FromTimestamp = inputText & "0101000000"
                Rtn = GetMstData(4, FromTimestamp)
                lblCountDown.Text = "取得待機"
                AllButtonControl(True)
            End If
        End If
    End Sub

    Private Sub JVDataGetStart_Click(sender As Object, e As EventArgs) Handles JVDataGetStart.Click
        Me.GetTimer.Start()
        StatusLabel.Text = String.Format("取得終了：自動取得待機中")
    End Sub

    Private Sub ProgramEnd_Click(sender As Object, e As EventArgs) Handles ProgramEnd.Click
        Me.Close()
    End Sub

    Private Sub HorseRacingTips_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
        Dim intRet As Integer
        intRet = MsgBox("終了しますか？", MsgBoxStyle.YesNo, "確認")
        If intRet = MsgBoxResult.No Then
            e.Cancel = True
        End If
    End Sub

    Private Sub ContextMenuStrip1_Click(sender As Object, e As EventArgs) Handles ContextMenuStrip1.Click
        HorseRacingTipsIcon.Visible = False   ' アイコンをトレイから取り除く
        Application.Exit()                    ' アプリケーションの終了
    End Sub

    Private Sub GetTimer_Tick(sender As Object, e As EventArgs) Handles GetTimer.Tick
        Dim Rtn1 As String
        'Dim Rtn2 As Long
        Dim strLabelCountDown As String
        Dim iRunnigCd As Integer

        'Timerの間隔を1000msecに再設定
        'Me.GetTimer.Interval = 1000
        If JVInitFlg = 0 Then
            strLabelCountDown = lblCountDown.Text
            'COM.LogOut(DateTime.Now & ":" & strLabelCountDown, False)
            iRunnigCd = -1
            If strLabelCountDown = "実行中！" Then
                'データ取得インターバル
                iRunnigCd = TimerInterval \ 1000
            Else
                If strLabelCountDown <> "取得待機" And Len(strLabelCountDown) <> 0 Then
                    Dim tmp As String = strLabelCountDown.Replace("秒後", "")
                    iRunnigCd = Integer.Parse(tmp)
                End If
            End If
            If iRunnigCd <= 0 Or strLabelCountDown = "取得待機" Then
                lblCountDown.Text = "実行中！"
                StatusLabel.Text = String.Format("取得中！")
                COM.LogOut("取得中")
                GetTimer.Enabled = False
                AllButtonControl(False)
                Dim dtToday As DateTime = DateTime.Now
                Dim dtToday_key As String
                'Dim dtYestrday As DateTime
                COM.LogOut("日付：" & dtToday, True)
                COM.LogOut("曜日：" & dtToday.DayOfWeek, True)
                'dtYestrday = dtToday.AddDays(-1)
                dtToday_key = Format(dtToday, "yyyyMMdd")
                '土日はリアルタイムで速報を取得
                If tmpToday <> dtToday_key Then
                    tmpToday = dtToday_key
                    MstGetFlag = False
                End If
                Select Case dtToday.DayOfWeek
                    Case DayOfWeek.Sunday, DayOfWeek.Saturday
                        Rtn1 = GetRTRaceData(dtToday_key)
                        Rtn1 = GetRTOddsData(dtToday_key)
                        Rtn1 = GetRTDMinigData(dtToday_key)
                        Rtn1 = GetRTTMinigData(dtToday_key)
                    Case Else
                        'If MstGetFlag = False And Format(dtToday, "HH:mm:ss") >= "01:00:00" Then
                        '    COM.LogOut("マスタ", True)
                        '    MstGetFlag = True
                        '    Rtn1 = GetMstData(1, dtToday_key & "000000")
                        '    'Rtn1 = GetMstData(1, "20201026000000")

                        'Else
                        COM.LogOut("レース", True)
                        Rtn1 = GetRaceData()
                        'End If
                End Select
                'COM.LogOut("Rtn1 = " & Rtn1)
                'If Rtn1 = 0 Then
                'Rtn2 = GetMstData(1, strFromTime)
                'End If
                AllButtonControl(True)
                StatusLabel.Text = String.Format("取得終了：自動取得待機中")
                COM.LogOut("取得終了：自動取得待機中")
                GetTimer.Enabled = True

                'Dim readValue = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\JRA-VAN Data Lab.\uid_pass", "savepath", Nothing)
                Dim readValue = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\JRA-VAN Data Lab.\uid_pass", "savepath", Nothing)
                COM.LogOut("The savepath is " & readValue, True)
                DeleteFolder(readValue & "\cache")
                DeleteFolder(readValue & "\data")

                strFromTime = strLastFileTimestamp
                COM.LogOut(strFromTime, False)
            Else
                iRunnigCd -= 1
                lblCountDown.Text = iRunnigCd & "秒後"
                GetTimer.Enabled = True
            End If
        End If
        COM.LogOut(Date.Now(), False)

    End Sub

    Private Function GetRaceData() As Long
        Dim lReturnCode As Long
        Dim strDataSpec As String '' 引数 JVOpen:ファイル識別子
        Dim lOption As Long '' 引数 JVOpen:オプション
        Dim lReadCount As Long '' JVLink 戻り値
        Dim lDownloadCount As Long '' JVOpen:総ダウンロードファイル数
        Dim strRecodeType As String '' レコード種別
        Dim strDataType As String '' データ種別
        Dim RecodeCnt As Integer 'レコード数
        Dim strHassoTime As String '発送時刻
        Const lBuffSize As Long = 110000 ''JVRead:データ格納バッファサイズ
        Const lNameSize As Integer = 256 ''JVRead:ファイル名サイズ
        Dim strBuff As String ''JVRead:データ格納バッファ
        Dim strFileName As String ''JVRead:ダウンロードファイル名
        Dim RaceInfo As JV_RA_RACE ''レース詳細情報構造体
        Dim RaceHorseInfo As JV_SE_RACE_UMA  ''馬毎レース情報構造体
        Dim HaraimodoshiInfo As JV_HR_PAY  ''払戻情報構造体
        'Dim OddsTanfukuwaku As JV_O1_ODDS_TANFUKUWAKU ''オッズ1（単複枠）構造体
        'Dim OddsUmaren As JV_O2_ODDS_UMAREN  ''オッズ2（馬連）構造体
        'Dim OddsWide As JV_O3_ODDS_WIDE  ''オッズ3（ワイド）構造体
        'Dim OddsUmatan As JV_O4_ODDS_UMATAN  ''オッズ4（馬単）構造体
        'Dim OddsSanrenfuku As JV_O5_ODDS_SANREN  ''オッズ5（3連複）構造体
        Dim OddsSanrentan As JV_O6_ODDS_SANRENTAN  ''オッズ6（3連単）構造体

        Dim RaceID As String ' レースＩＤ
        Dim Month As String '開催月
        Dim Day As String '開催日
        Dim RaceName_hon As String '競争名本題
        Dim RaceName_rya As String '競争名略名
        Dim Racetyoe As String '
        Dim Jouken As String '
        Dim Racekigo As String '
        Dim RaceName As String '競争名
        Dim DbConnectFlg As Boolean 'DB接続フラグ
        Dim FtpEndFile As String

        RecodeCnt = 0 'レコード数

        '引数設定
        strDataSpec = "RACE" 'レコード種別（蓄積情報）
        lOption = "2" ' 今週データ
        COM.LogOut("Opt:" & lOption & " FromTime:" & strFromTime, True)

        Me.Cursor = Cursors.WaitCursor
        Dim ra_cnt As Long
        Dim se_cnt As Long
        ra_cnt = se_cnt = 0

        Try
            'レースデータダウンロード
            '-----------------------------------------------------------------------------------------
            'JVLink ダウンロード処理
            lReturnCode = Me.AxJVLink1.JVOpen(strDataSpec, strFromTime, lOption, lReadCount, lDownloadCount, strLastFileTimestamp)
            'エラー判定
            If lReturnCode = 0 Then
                StatusLabel.Text = String.Format("ダウンロードファイル数 :" & lDownloadCount)
                'Console.Write("最新ダウンロードタイム：" & strLastFileTimestamp)

                If lReadCount > 0 Then
                    Do
                        'バックグラウンドでの処理を実行
                        Application.DoEvents()
                        'Query宣言
                        Dim SelectQuery As String
                        Dim InsertQuery As String
                        Dim UpdateQuery As String
                        '競走馬情報
                        Dim SelectQueryH As String
                        Dim InsertQueryH As String
                        Dim UpdateQueryH As String
                        ''調教師情報
                        'Dim SelectQueryC As String
                        'Dim InsertQueryC As String
                        'Dim UpdateQueryC As String
                        ''騎手情報
                        Dim SelectQueryJ As String
                        Dim InsertQueryJ As String
                        Dim UpdateQueryJ As String
                        '払戻情報
                        Dim SelectQueryP As String
                        Dim InsertQueryP As String
                        Dim UpdateQueryP As String
                        'オッズ情報
                        Dim SelectQueryO As String
                        Dim InsertQueryO As String
                        Dim UpdateQueryO As String

                        SelectQuery = ""
                        InsertQuery = ""
                        UpdateQuery = ""
                        SelectQueryH = ""
                        InsertQueryH = ""
                        UpdateQueryH = ""
                        'SelectQueryC = ""
                        'InsertQueryC = ""
                        'UpdateQueryC = ""
                        SelectQueryJ = ""
                        InsertQueryJ = ""
                        UpdateQueryJ = ""
                        SelectQueryP = ""
                        InsertQueryP = ""
                        UpdateQueryP = ""
                        SelectQueryO = ""
                        InsertQueryO = ""
                        UpdateQueryO = ""

                        '構造体初期化
                        RaceInfo = New JV_RA_RACE
                        RaceHorseInfo = New JV_SE_RACE_UMA
                        HaraimodoshiInfo = New JV_HR_PAY
                        'OddsTanfukuwaku = New JV_O1_ODDS_TANFUKUWAKU
                        'OddsUmaren = New JV_O2_ODDS_UMAREN
                        'OddsWide = New JV_O3_ODDS_WIDE
                        'OddsUmatan = New JV_O4_ODDS_UMATAN
                        'OddsSanrenfuku = New JV_O5_ODDS_SANREN
                        OddsSanrentan = New JV_O6_ODDS_SANRENTAN

                        'バッファ作成
                        strBuff = New String(vbNullChar, lBuffSize)
                        strFileName = New String(vbNullChar, lNameSize)
                        'JVRead で１行読み込み
                        lReturnCode = Me.AxJVLink1.JVRead(strBuff, lBuffSize, strFileName)
                        'COM.LogOut(lReturnCode & ":" & strBuff & ":" & lBuffSize & ":" & strFileName, False)

                        'リターンコードにより処理を分枝
                        DbConnectFlg = False
                        Select Case lReturnCode
                            Case 0 ' 全ファイル読み込み終了
                                StatusLabel.Text = String.Format("レースデータ終了")
                                Exit Do
                            Case -1 ' ファイル切り替わり
                            Case -3 ' ダウンロード中
                            Case -201 ' Init されてない
                                MsgBox("JVInit が行われていません。")
                                Exit Do
                            Case -203 ' Open されてない
                                MsgBox("JVOpen が行われていません。")
                                Exit Do
                            Case -503 ' ファイルがない
                                MsgBox(strFileName & "が存在しません。")
                                Exit Do
                            Case Is > 0 ' 正常読み込み
                                'レコード種別 ID の識別
                                strRecodeType = Mid(strBuff, 1, 2)
                                strDataType = Mid(strBuff, 3, 1)
                                COM.LogOut(strRecodeType & ":" & strDataType, False)
                                If strRecodeType = "RA" Then
                                    ra_cnt += 1
                                    StatusLabel.Text = String.Format("RA:" & ra_cnt)
                                    'レース詳細のみ処理 'レース詳細構造体への展開
                                    RaceInfo.SetData(strBuff)
                                    'データ表示
                                    RaceID = RaceInfo.id.Year & RaceInfo.id.MonthDay & RaceInfo.id.JyoCD &
                                          RaceInfo.id.Kaiji & RaceInfo.id.Nichiji & RaceInfo.id.RaceNum
                                    Month = Mid(RaceInfo.id.MonthDay, 1, 2)
                                    Day = Mid(RaceInfo.id.MonthDay, 3, 2)
                                    COM.LogOut("[RA]RaceID:" & RaceID, True)
                                    RaceName_hon = Trim(RaceInfo.RaceInfo.Hondai) & " " & Trim(RaceInfo.RaceInfo.Fukudai)
                                    '競走種別コード(2005)の1列目で変換
                                    '競走記号コード(2006)の1列目で変換
                                    '競走条件コード(2007)の1列目で変換
                                    Racetyoe = objCodeConv.GetCodeName("2005", RaceInfo.JyokenInfo.SyubetuCD, "1")
                                    Racekigo = objCodeConv.GetCodeName("2006", RaceInfo.JyokenInfo.KigoCD, "1")
                                    Jouken = ""
                                    For i As Integer = 0 To 4
                                        'COM.LogOut("競争条件（" & i & "):" & RaceInfo.JyokenInfo.JyokenCD(i))
                                        If RaceInfo.JyokenInfo.JyokenCD(i) <> "000" Then
                                            Jouken = objCodeConv.GetCodeName("2007", RaceInfo.JyokenInfo.JyokenCD(i), "1")
                                            Exit For
                                        End If
                                    Next i
                                    RaceName_rya = Racetyoe & Jouken & Racekigo
                                    'COM.LogOut("RaceName_hon:" & RaceName_hon)
                                    'COM.LogOut("RaceName_rya:" & RaceName_rya)
                                    If Len(Trim(RaceName_hon)) = 0 Then
                                        RaceName = RaceName_rya
                                    Else
                                        RaceName = RaceName_hon
                                    End If
                                    If Len(Trim(RaceName)) = 0 Then
                                        Dim Jyo As String
                                        Jyo = objCodeConv.GetCodeName("2001", RaceInfo.id.JyoCD, "4")
                                        RaceName = Jyo & RaceInfo.id.Kaiji & "回" & RaceInfo.id.Nichiji & "日目" _
                                                 & "第" & RaceInfo.id.RaceNum & "レース"
                                    End If
                                    COM.LogOut("[RA]RaceName:" & RaceName, True)
                                    StatusLabel.Text = String.Format("RA:" & ra_cnt & ",Race:" & RaceID)
                                    strHassoTime = Mid(RaceInfo.HassoTime, 1, 2) & ":" & Mid(RaceInfo.HassoTime, 3, 2)
                                    'クエリー生成
                                    SelectQuery = "SELECT DataType FROM RaceTbl WHERE RaceID = '" & RaceID & "'"
                                    InsertQuery = "INSERT INTO RaceTbl VALUES ('" & RaceID & "'" _
                                                                    & ",'" & RaceInfo.id.Year & "'" _
                                                                    & ",'" & Month & "'" _
                                                                    & ",'" & Day & "'" _
                                                                    & ",'" & RaceInfo.id.JyoCD & "'" _
                                                                    & ",'" & RaceInfo.id.Kaiji & "'" _
                                                                    & ",'" & RaceInfo.id.Nichiji & "'" _
                                                                    & ",'" & RaceInfo.id.RaceNum & "'" _
                                                                    & ",'" & RaceName & "'" _
                                                                    & ", " & RaceInfo.Kyori _
                                                                    & ",'" & strHassoTime & "'" _
                                                                    & ",'" & RaceInfo.TrackCD & "'" _
                                                                    & ",'" & RaceInfo.TorokuTosu & "'" _
                                                                    & ",'" & RaceInfo.SyussoTosu & "'" _
                                                                    & ",'" & RaceInfo.TenkoBaba.TenkoCD & "'" _
                                                                    & ",'" & RaceInfo.TenkoBaba.SibaBabaCD & "'" _
                                                                    & ",'" & RaceInfo.TenkoBaba.DirtBabaCD & "'" _
                                                                    & ",'" & strDataType & "'" _
                                                                    & ", now(), null)"
                                    UpdateQuery = "UPDATE RaceTbl SET RaceName = '" & RaceName & "'" _
                                                        & ", Year = '" & RaceInfo.id.Year & "'" _
                                                        & ", Month = '" & Month & "'" _
                                                        & ", Day = '" & Day & "'" _
                                                        & ", Course = '" & RaceInfo.id.JyoCD & "'" _
                                                        & ", Kaiji = '" & RaceInfo.id.Kaiji & "'" _
                                                        & ", Kaiday = '" & RaceInfo.id.Nichiji & "'" _
                                                        & ", RaceNo = '" & RaceInfo.id.RaceNum & "'" _
                                                        & ", Distance = " & RaceInfo.Kyori _
                                                        & ", HassoTime = '" & strHassoTime & "'" _
                                                        & ", TrackCD = '" & RaceInfo.TrackCD & "'" _
                                                        & ", RegistHorse = '" & RaceInfo.TorokuTosu & "'" _
                                                        & ", StartHorse = '" & RaceInfo.SyussoTosu & "'" _
                                                        & ", Weather = '" & RaceInfo.TenkoBaba.TenkoCD & "'" _
                                                        & ", SibaCD = '" & RaceInfo.TenkoBaba.SibaBabaCD & "'" _
                                                        & ", DirtCD = '" & RaceInfo.TenkoBaba.DirtBabaCD & "'" _
                                                        & ", DataType = '" & strDataType & "'" _
                                                        & ", modified = now() WHERE RaceID = '" & RaceID & "'"

                                    DbConnectFlg = True
                                ElseIf strRecodeType = "SE" Then
                                    '馬毎レースのみ処理
                                    '馬毎レース構造体への展開
                                    RaceHorseInfo.SetData(strBuff)
                                    se_cnt += 1
                                    'データ表示
                                    RaceID = RaceHorseInfo.id.Year & RaceHorseInfo.id.MonthDay & RaceHorseInfo.id.JyoCD &
                                        RaceHorseInfo.id.Kaiji & RaceHorseInfo.id.Nichiji & RaceHorseInfo.id.RaceNum
                                    StatusLabel.Text = String.Format("SE:" & se_cnt & ",Race:" & RaceID & ",HorseID:" & RaceHorseInfo.KettoNum)
                                    COM.LogOut("[SE]RaceID:" & RaceID, True)
                                    'クエリー生成
                                    SelectQuery = "SELECT DataType FROM RaceDetailTbl" _
                                                                    & " WHERE RaceID = '" & RaceID & "'" _
                                                                    & " AND HorseID = '" & RaceHorseInfo.KettoNum & "'"
                                    InsertQuery = "INSERT INTO RaceDetailTbl VALUES ('" & RaceID & "'" _
                                                                    & ",'" & RaceHorseInfo.KettoNum & "'" _
                                                                    & ",'" & RaceHorseInfo.Barei & "'" _
                                                                    & ",'" & RaceHorseInfo.ChokyosiCode & "'" _
                                                                    & ",'" & RaceHorseInfo.KisyuCode & "'" _
                                                                    & ",'" & RaceHorseInfo.Wakuban & "'" _
                                                                    & ",'" & RaceHorseInfo.Umaban & "'" _
                                                                    & ",'" & RaceHorseInfo.IJyoCD & "'" _
                                                                    & ",'" & RaceHorseInfo.KakuteiJyuni & "'" _
                                                                    & ",'" & RaceHorseInfo.ChakusaCD & "'" _
                                                                    & ",'" & strDataType & "'" _
                                                                    & ", now(), null)"
                                    UpdateQuery = "UPDATE RaceDetailTbl SET HorseAge = '" & RaceHorseInfo.Barei & "'" _
                                                        & ", TrainerID = '" & RaceHorseInfo.ChokyosiCode & "'" _
                                                        & ", JockeyID = '" & RaceHorseInfo.KisyuCode & "'" _
                                                        & ", Wakuban = '" & RaceHorseInfo.Wakuban & "'" _
                                                        & ", Umaban = '" & RaceHorseInfo.Umaban & "'" _
                                                        & ", HorseStatus = '" & RaceHorseInfo.IJyoCD & "'" _
                                                        & ", RaceResult = '" & RaceHorseInfo.KakuteiJyuni & "'" _
                                                        & ", Difference = '" & RaceHorseInfo.ChakusaCD & "'" _
                                                        & ", DataType = '" & strDataType & "'" _
                                                        & ", modified = now()" _
                                                        & " WHERE RaceID = '" & RaceID & "'" _
                                                        & " AND HorseID = '" & RaceHorseInfo.KettoNum & "'"

                                    SelectQueryH = "SELECT HorseName FROM HorseTbl WHERE HorseID = " & RaceHorseInfo.KettoNum
                                    InsertQueryH = "INSERT INTO HorseTbl VALUES (" & RaceHorseInfo.KettoNum _
                                                                    & ", '" & Trim(RaceHorseInfo.Bamei) _
                                                                    & "', '', '' , now(), null)"
                                    UpdateQueryH = "UPDATE HorseTbl SET HorseName = '" & Trim(RaceHorseInfo.Bamei) _
                                                            & "', modified = now()" _
                                                            & " WHERE HorseID = " & RaceHorseInfo.KettoNum

                                    'SelectQueryC = "SELECT TrainerName FROM TrainerTbl WHERE TrainerCD = '" & RaceHorseInfo.ChokyosiCode & "'"
                                    'InsertQueryC = "INSERT INTO TrainerTbl VALUES ('" & RaceHorseInfo.ChokyosiCode _
                                    '                                        & "', '" & Trim(RaceHorseInfo.ChokyosiRyakusyo) _
                                    '                                        & "', '', '' , now(), null)"
                                    'UpdateQueryC = "UPDATE TrainerTbl SET TrainerName = '" & Trim(RaceHorseInfo.ChokyosiRyakusyo) _
                                    '                                & "', modified = now()" _
                                    '                                & " WHERE TrainerCD = '" & RaceHorseInfo.ChokyosiCode & "'"

                                    SelectQueryJ = "SELECT JockeyName FROM JockeyTbl WHERE JockeyCD = '" & RaceHorseInfo.KisyuCode & "'"
                                    InsertQueryJ = "INSERT INTO JockeyTbl VALUES ('" & RaceHorseInfo.KisyuCode _
                                                                            & "', '" & Trim(RaceHorseInfo.KisyuRyakusyo) _
                                                                            & "', '', '' , 0, now(), null)"
                                    UpdateQueryJ = "UPDATE JockeyTbl SET JockeyName = '" & Trim(RaceHorseInfo.KisyuRyakusyo) _
                                                                    & "', modified = now()" _
                                                                    & " WHERE JockeyCD = '" & RaceHorseInfo.KisyuCode & "'"

                                    DbConnectFlg = True
                                ElseIf strRecodeType = "HR" Then
                                    '払戻構造体への展開
                                    HaraimodoshiInfo.SetData(strBuff)
                                    'データ表示
                                    RaceID = HaraimodoshiInfo.id.Year & HaraimodoshiInfo.id.MonthDay & HaraimodoshiInfo.id.JyoCD &
                                          HaraimodoshiInfo.id.Kaiji & HaraimodoshiInfo.id.Nichiji & HaraimodoshiInfo.id.RaceNum
                                    'COM.LogOut("RaceID:" & RaceID)
                                    'COM.LogOut("馬番:" & HaraimodoshiInfo.PayTansyo(0).Umaban & "(" & HaraimodoshiInfo.PayTansyo(0).Ninki & "):" & HaraimodoshiInfo.PayTansyo(0).Pay)
                                    'COM.LogOut("複勝１:" & HaraimodoshiInfo.PayFukusyo(0).Umaban & "(" & HaraimodoshiInfo.PayFukusyo(0).Ninki & "):" & HaraimodoshiInfo.PayFukusyo(0).Pay)
                                    'COM.LogOut("複勝２:" & HaraimodoshiInfo.PayFukusyo(1).Umaban & "(" & HaraimodoshiInfo.PayFukusyo(1).Ninki & "):" & HaraimodoshiInfo.PayFukusyo(1).Pay)
                                    'COM.LogOut("複勝３:" & HaraimodoshiInfo.PayFukusyo(2).Umaban & "(" & HaraimodoshiInfo.PayFukusyo(2).Ninki & "):" & HaraimodoshiInfo.PayFukusyo(2).Pay)
                                    'COM.LogOut("枠連１:" & Mid(HaraimodoshiInfo.PayWakuren(0).Umaban, 1, 2) & "-" & Mid(HaraimodoshiInfo.PayWakuren(0).Umaban, 3, 2) & "(" & HaraimodoshiInfo.PayWakuren(0).Ninki & "):" &
                                    '                       HaraimodoshiInfo.PayWakuren(0).Pay)
                                    'COM.LogOut("馬連１:" & Mid(HaraimodoshiInfo.PayUmaren(0).Kumi, 1, 2) & "-" & Mid(HaraimodoshiInfo.PayUmaren(0).Kumi, 3, 2) & "(" & HaraimodoshiInfo.PayUmaren(0).Ninki & "):" &
                                    '                       HaraimodoshiInfo.PayUmaren(0).Pay)
                                    'COM.LogOut("ワイド１:" & Mid(HaraimodoshiInfo.PayWide(0).Kumi, 1, 2) & "-" & Mid(HaraimodoshiInfo.PayWide(0).Kumi, 3, 2) & "(" & HaraimodoshiInfo.PayWide(0).Ninki & "):" &
                                    '                       HaraimodoshiInfo.PayWide(0).Pay)
                                    'COM.LogOut("ワイド２:" & Mid(HaraimodoshiInfo.PayWide(1).Kumi, 1, 2) & "-" & Mid(HaraimodoshiInfo.PayWide(1).Kumi, 3, 2) & "(" & HaraimodoshiInfo.PayWide(1).Ninki & "):" &
                                    '                       HaraimodoshiInfo.PayWide(1).Pay)
                                    'COM.LogOut("ワイド３:" & Mid(HaraimodoshiInfo.PayWide(2).Kumi, 1, 2) & "-" & Mid(HaraimodoshiInfo.PayWide(2).Kumi, 3, 2) & "(" & HaraimodoshiInfo.PayWide(2).Ninki & "):" &
                                    '                       HaraimodoshiInfo.PayWide(2).Pay)
                                    'COM.LogOut("馬単１:" & Mid(HaraimodoshiInfo.PayUmatan(0).Kumi, 1, 2) & "-" & Mid(HaraimodoshiInfo.PayUmatan(0).Kumi, 3, 2) & "(" & HaraimodoshiInfo.PayUmatan(0).Ninki & "):" &
                                    '                       HaraimodoshiInfo.PayUmatan(0).Pay)
                                    'COM.LogOut("３連複１:" & Mid(HaraimodoshiInfo.PaySanrenpuku(0).Kumi, 1, 2) & "-" & Mid(HaraimodoshiInfo.PaySanrenpuku(0).Kumi, 3, 2) & "-" & Mid(HaraimodoshiInfo.PaySanrenpuku(0).Kumi, 5, 2) & "(" & HaraimodoshiInfo.PaySanrenpuku(0).Ninki & "):" &
                                    '                       HaraimodoshiInfo.PaySanrenpuku(0).Pay)
                                    'COM.LogOut("３連単１:" & Mid(HaraimodoshiInfo.PaySanrentan(0).Kumi, 1, 2) & "-" & Mid(HaraimodoshiInfo.PaySanrentan(0).Kumi, 3, 2) & "-" & Mid(HaraimodoshiInfo.PaySanrentan(0).Kumi, 5, 2) & "(" & HaraimodoshiInfo.PaySanrentan(0).Ninki & "):" &
                                    '                       HaraimodoshiInfo.PaySanrentan(0).Pay)
                                    'クエリー生成
                                    SelectQueryP = "SELECT DataType FROM PayTbl" _
                                                                    & " WHERE RaceID = '" & RaceID & "'"
                                    InsertQueryP = "INSERT INTO PayTbl VALUES ('" & RaceID & "'" _
                                                                    & ",'" & StrToInt(HaraimodoshiInfo.PayTansyo(0).Pay) & "'" _
                                                                    & ",'" & StrToInt(HaraimodoshiInfo.PayFukusyo(0).Pay) & "'" _
                                                                    & ",'" & StrToInt(HaraimodoshiInfo.PayFukusyo(1).Pay) & "'" _
                                                                    & ",'" & StrToInt(HaraimodoshiInfo.PayFukusyo(2).Pay) & "'" _
                                                                    & ",'" & StrToInt(HaraimodoshiInfo.PayWakuren(0).Pay) & "'" _
                                                                    & ",'" & StrToInt(HaraimodoshiInfo.PayUmaren(0).Pay) & "'" _
                                                                    & ",'" & StrToInt(HaraimodoshiInfo.PayWide(0).Pay) & "'" _
                                                                    & ",'" & StrToInt(HaraimodoshiInfo.PayWide(1).Pay) & "'" _
                                                                    & ",'" & StrToInt(HaraimodoshiInfo.PayWide(2).Pay) & "'" _
                                                                    & ",'" & StrToInt(HaraimodoshiInfo.PayUmatan(0).Pay) & "'" _
                                                                    & ",'" & StrToInt(HaraimodoshiInfo.PaySanrenpuku(0).Pay) & "'" _
                                                                    & ",'" & StrToInt(HaraimodoshiInfo.PaySanrentan(0).Pay) & "'" _
                                                                    & ",'" & strDataType & "'" _
                                                                    & ", now(), null)"
                                    UpdateQueryP = "UPDATE PayTbl SET Tansyo = '" & StrToInt(HaraimodoshiInfo.PayTansyo(0).Pay) & "'" _
                                                        & ", Fukusyo1 = '" & StrToInt(HaraimodoshiInfo.PayFukusyo(0).Pay) & "'" _
                                                        & ", Fukusyo2 = '" & StrToInt(HaraimodoshiInfo.PayFukusyo(1).Pay) & "'" _
                                                        & ", Fukusyo3 = '" & StrToInt(HaraimodoshiInfo.PayFukusyo(2).Pay) & "'" _
                                                        & ", Wakuren = '" & StrToInt(HaraimodoshiInfo.PayWakuren(0).Pay) & "'" _
                                                        & ", Umaren = '" & StrToInt(HaraimodoshiInfo.PayUmaren(0).Pay) & "'" _
                                                        & ", Wide1 = '" & StrToInt(HaraimodoshiInfo.PayWide(0).Pay) & "'" _
                                                        & ", Wide2 = '" & StrToInt(HaraimodoshiInfo.PayWide(1).Pay) & "'" _
                                                        & ", Wide3 = '" & StrToInt(HaraimodoshiInfo.PayWide(2).Pay) & "'" _
                                                        & ", Umatan = '" & StrToInt(HaraimodoshiInfo.PayUmatan(0).Pay) & "'" _
                                                        & ", SanrenFuku = '" & StrToInt(HaraimodoshiInfo.PaySanrenpuku(0).Pay) & "'" _
                                                        & ", Sanrentan = '" & StrToInt(HaraimodoshiInfo.PaySanrentan(0).Pay) & "'" _
                                                        & ", DataType = '" & strDataType & "'" _
                                                        & ", modified = now()" _
                                                        & " WHERE RaceID = '" & RaceID & "'"

                                    DbConnectFlg = True
                                ElseIf strRecodeType = "O6" Then
                                    Dim putFilename As String
                                    '３連単構造体への展開
                                    OddsSanrentan.SetData(strBuff)
                                    'COM.LogOut("OddsSanrentan Full:" & strBuff)
                                    'データ表示
                                    RaceID = OddsSanrentan.id.Year & OddsSanrentan.id.MonthDay & OddsSanrentan.id.JyoCD &
                                                OddsSanrentan.id.Kaiji & OddsSanrentan.id.Nichiji & OddsSanrentan.id.RaceNum
                                    StatusLabel.Text = String.Format("O6:" & ",Race:" & RaceID)
                                    'COM.LogOut("Happyo:" & OddsSanrentan.HappyoTime.Month & OddsSanrentan.HappyoTime.Day & OddsSanrentan.HappyoTime.Hour & OddsSanrentan.HappyoTime.Minute)
                                    putFilename = OddsFileOutput(RaceID, strBuff)
                                    PutOddsFile(putFilename)
                                Else
                                    '上記以外の
                                    Me.AxJVLink1.JVSkip()
                                End If
                                If DbConnectFlg = True Then
                                    'MySQLコネクション生成
                                    DB.dbConnect(connectionString)
                                    Dim rc As String
                                    Try
                                        'MySQL接続
                                        DB.dbConnectionOpen()
                                        If Len(SelectQuery) <> 0 Then
                                            '既存レコードの検索
                                            rc = DB.dbSelectDataRead(SelectQuery)
                                            COM.LogOut("Datatype=" & rc & ":" & strDataType, True)
                                            '更新系クエリー
                                            If Len(rc) > 0 Then
                                                If rc = "-1" Then
                                                    DB.dbUpdate(InsertQuery)
                                                ElseIf rc < strDataType Or strDataType = 0 Then
                                                    DB.dbUpdate(UpdateQuery)
                                                End If
                                            End If
                                        End If
                                        If Len(SelectQueryH) <> 0 Then
                                            'COM.LogOut("競走馬", False)
                                            '競走馬情報登録
                                            '既存レコードの検索
                                            rc = DB.dbSelectDataRead(SelectQueryH)
                                            If Len(rc) > 0 Then
                                                If rc = "-1" Then
                                                    DB.dbUpdate(InsertQueryH)
                                                End If
                                            End If
                                        End If
                                        If Len(SelectQueryP) <> 0 Then
                                            '既存レコードの検索
                                            COM.LogOut(SelectQueryP)
                                            rc = DB.dbSelectDataRead(SelectQueryP)
                                            If Len(rc) > 0 Then
                                                If rc = "-1" Then
                                                    DB.dbUpdate(InsertQueryP)
                                                End If
                                            End If
                                        End If
                                        'If Len(SelectQueryC) <> 0 Then
                                        '    COM.LogOut("調教師", False)
                                        '    '調教師情報登録
                                        '    '既存レコードの検索
                                        '    rc = DB.dbSelectDataRead(SelectQueryC)
                                        '    If Len(rc) > 0 Then
                                        '        If rc = "-1" Then
                                        '            DB.dbUpdate(InsertQueryC)
                                        '        End If
                                        '    End If
                                        'End If
                                        If Len(SelectQueryJ) <> 0 Then
                                            '騎手情報登録
                                            '既存レコードの検索
                                            rc = DB.dbSelectDataRead(SelectQueryJ)
                                            If Len(rc) > 0 Then
                                                If rc = "-1" Then
                                                    DB.dbUpdate(InsertQueryJ)
                                                End If
                                            End If
                                        End If
                                        'MySQL切断
                                        DB.dbConnectionClose()
                                    Catch e As MySqlException
                                        MsgBox("データベース接続に失敗しました。(" & DbData.DbName & ")" _
                                               & vbCrLf & "(" & e.ErrorCode & ")" & e.Message)
                                        'JVLink 終了処理
                                        lReturnCode = Me.AxJVLink1.JVClose()
                                        If lReturnCode <> 0 Then
                                            MsgBox("JVClose エラー：" & lReturnCode)
                                            Return -301
                                        End If
                                        Me.Cursor = Cursors.Default
                                        Return -300
                                    End Try
                                End If
                        End Select
                    Loop While (1)
                    ' 転送ファイル終了ファイルを転送後削除
                    FtpEndFile = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\JRA-VAN Data Lab.\uid_pass", "savepath", Nothing) & "\odds.end"
                    If File.Exists(FtpEndFile) Then
                        PutOddsFile(FtpEndFile)
                        File.Delete(FtpEndFile)
                    End If
                End If
            ElseIf lReturnCode = -1 Then
                Dim result As DialogResult
                result = CreateObject("WScript.Shell").Popup("最新です", 2, "")
                COM.LogOut("最新", True)
                strLastFileTimestamp = strFromTime
            ElseIf lReturnCode = -504 Then
                COM.LogOut("メンテナンス中R：" & lReturnCode & "(" & strDataSpec & ":" & strFromTime & ")", True)
                strLastFileTimestamp = strFromTime
            Else
                strLastFileTimestamp = strFromTime
                MsgBox("JVOpen エラーR：" & lReturnCode & "(" & strDataSpec & ":" & strFromTime & ")")
            End If
        Catch
            Debug.WriteLine(Err.Description)
            COM.LogOut("例外:" & Err.Description, True)
            Me.Cursor = Cursors.Default
            Return -100
        End Try
        'JVLink 終了処理
        lReturnCode = Me.AxJVLink1.JVClose()
        If lReturnCode <> 0 Then
            MsgBox("JVClose エラー：" & lReturnCode)
            Return -200
        End If
        Me.Cursor = Cursors.Default
        Return 0
    End Function

    Private Function GetMstData(ByVal lOption As Long, ByVal strMstFromTime As String) As Long
        Dim lReturnCode As Long
        Dim strDataSpec As String '' 引数 JVOpen:ファイル識別子
        Dim lReadCount As Long '' JVLink 戻り値
        Dim lDownloadCount As Long '' JVOpen:総ダウンロードファイル数
        Dim strRecodeType As String '' レコード種別
        Dim strDataType As String '' データ種別
        Dim RecodeCnt As Integer 'レコード数
        Dim strHassoTime As String '発送時刻
        Const lBuffSize As Long = 110000 ''JVRead:データ格納バッファサイズ
        Const lNameSize As Integer = 256 ''JVRead:ファイル名サイズ
        Dim strBuff As String ''JVRead:データ格納バッファ
        Dim strFileName As String ''JVRead:ダウンロードファイル名
        Dim RaceInfo As JV_RA_RACE ''レース詳細情報構造体
        Dim RaceHorseInfo As JV_SE_RACE_UMA  ''馬毎レース情報構造体
        Dim HaraimodoshiInfo As JV_HR_PAY  ''払戻情報構造体
        Dim RaceID As String ' レースＩＤ
        Dim Month As String '開催月
        Dim Day As String '開催日
        Dim RaceName_hon As String '競争名本題
        Dim RaceName_rya As String '競争名略名
        Dim Racetyoe As String '
        Dim Jouken As String '
        Dim Racekigo As String '
        Dim RaceName As String '競争名
        Dim DbConnectFlg As Boolean 'DB接続フラグ
        RecodeCnt = 0 'レコード数

        '引数設定
        strDataSpec = "RACE" 'レコード種別（蓄積情報）
        COM.LogOut("Opt:" & lOption & "  FromTime:" & strMstFromTime, True)
        Me.Cursor = Cursors.WaitCursor
        Dim i As Long
        Dim ra_cnt As Long
        Dim se_cnt As Long
        Dim hr_cnt As Long
        i = ra_cnt = se_cnt = hr_cnt = 0

        Try
            'レースデータダウンロード
            '-----------------------------------------------------------------------------------------
            'JVLink ダウンロード処理
            lReturnCode = Me.AxJVLink1.JVOpen(strDataSpec, strMstFromTime, lOption, lReadCount, lDownloadCount, strLastFileMstTimestamp)
            'エラー判定
            If lReturnCode = 0 Then
                StatusLabel.Text = String.Format("ダウンロードファイル数 :" & lDownloadCount)
                'Lesson 3追加
                If lReadCount > 0 Then
                    Do
                        'バックグラウンドでの処理を実行
                        Application.DoEvents()

                        'Query宣言
                        Dim SelectQuery As String
                        Dim InsertQuery As String
                        Dim UpdateQuery As String
                        '競走馬情報
                        Dim SelectQueryH As String
                        Dim InsertQueryH As String
                        Dim UpdateQueryH As String
                        '騎手情報
                        Dim SelectQueryJ As String
                        Dim InsertQueryJ As String
                        Dim UpdateQueryJ As String
                        '払戻情報
                        Dim SelectQueryP As String
                        Dim InsertQueryP As String
                        Dim UpdateQueryP As String

                        SelectQuery = ""
                        InsertQuery = ""
                        UpdateQuery = ""
                        SelectQueryH = ""
                        InsertQueryH = ""
                        UpdateQueryH = ""
                        SelectQueryJ = ""
                        InsertQueryJ = ""
                        UpdateQueryJ = ""
                        SelectQueryP = ""
                        InsertQueryP = ""
                        UpdateQueryP = ""

                        '構造体初期化
                        RaceInfo = New JV_RA_RACE
                        RaceHorseInfo = New JV_SE_RACE_UMA
                        HaraimodoshiInfo = New JV_HR_PAY
                        'HorseMaster = New JV_UM_UMA
                        'JockeyMaster = New JV_KS_KISYU
                        'TrainerMaster = New JV_CH_CHOKYOSI

                        'バッファ作成
                        strBuff = New String(vbNullChar, lBuffSize)
                        strFileName = New String(vbNullChar, lNameSize)
                        'JVRead で１行読み込み
                        lReturnCode = Me.AxJVLink1.JVRead(strBuff, lBuffSize, strFileName)
                        'リターンコードにより処理を分枝
                        DbConnectFlg = False
                        Select Case lReturnCode
                            Case 0 ' 全ファイル読み込み終了
                                StatusLabel.Text = String.Format("マスタデータ終了")
                                Exit Do
                            Case -1 ' ファイル切り替わり
                            Case -3 ' ダウンロード中
                            Case -201 ' Init されてない
                                MsgBox("JVInit が行われていません。")
                                Exit Do
                            Case -203 ' Open されてない
                                MsgBox("JVOpen が行われていません。")
                                Exit Do
                            Case -503 ' ファイルがない
                                MsgBox(strFileName & "が存在しません。")
                                Exit Do
                            Case Is > 0 ' 正常読み込み
                                'レコード種別 ID の識別
                                strRecodeType = Mid(strBuff, 1, 2)
                                strDataType = Mid(strBuff, 3, 1)
                                COM.LogOut(strRecodeType & ":" & strDataType, False)
                                'レコード種別 ID の識別
                                If strRecodeType = "RA" Then
                                    ' 中央競馬（確定）競馬
                                    If strDataType = "7" Then
                                        ra_cnt += 1
                                        'レース詳細のみ処理 'レース詳細構造体への展開
                                        RaceInfo.SetData(strBuff)
                                        'データ表示
                                        RaceID = RaceInfo.id.Year & RaceInfo.id.MonthDay & RaceInfo.id.JyoCD &
                                          RaceInfo.id.Kaiji & RaceInfo.id.Nichiji & RaceInfo.id.RaceNum
                                        Month = Mid(RaceInfo.id.MonthDay, 1, 2)
                                        Day = Mid(RaceInfo.id.MonthDay, 3, 2)
                                        COM.LogOut("[RA]RaceID:" & RaceID, True)
                                        RaceName_hon = Trim(RaceInfo.RaceInfo.Hondai) & " " & Trim(RaceInfo.RaceInfo.Fukudai)
                                        '競走種別コード(2005)の1列目で変換
                                        '競走記号コード(2006)の1列目で変換
                                        '競走条件コード(2007)の1列目で変換
                                        Racetyoe = objCodeConv.GetCodeName("2005", RaceInfo.JyokenInfo.SyubetuCD, "1")
                                        Racekigo = objCodeConv.GetCodeName("2006", RaceInfo.JyokenInfo.KigoCD, "1")
                                        Jouken = ""
                                        For ii As Integer = 0 To 4
                                            COM.LogOut("競争条件（" & ii & "):" & RaceInfo.JyokenInfo.JyokenCD(ii))
                                            If RaceInfo.JyokenInfo.JyokenCD(ii) <> "000" Then
                                                Jouken = objCodeConv.GetCodeName("2007", RaceInfo.JyokenInfo.JyokenCD(ii), "1")
                                                Exit For
                                            End If
                                        Next ii
                                        RaceName_rya = Racetyoe & Jouken & Racekigo
                                        'COM.LogOut("RaceName_hon:" & RaceName_hon)
                                        COM.LogOut("RaceName_rya:" & RaceName_rya)
                                        If Len(Trim(RaceName_hon)) = 0 Then
                                            RaceName = RaceName_rya
                                        Else
                                            RaceName = RaceName_hon
                                        End If
                                        If Len(Trim(RaceName)) = 0 Then
                                            Dim Jyo As String
                                            Jyo = objCodeConv.GetCodeName("2001", RaceInfo.id.JyoCD, "4")
                                            COM.LogOut(RaceInfo.id.JyoCD & ":" & Jyo)
                                            RaceName = Jyo & RaceInfo.id.Kaiji & "回" & RaceInfo.id.Nichiji & "日目" _
                                                 & "第" & RaceInfo.id.RaceNum & "レース"
                                        End If
                                        COM.LogOut("[RA]RaceName:" & RaceName, True)
                                        StatusLabel.Text = String.Format("RA:" & ra_cnt & ",Race:" & RaceID)
                                        strHassoTime = Mid(RaceInfo.HassoTime, 1, 2) & ":" & Mid(RaceInfo.HassoTime, 3, 2)
                                        'クエリー生成
                                        SelectQuery = "SELECT DataType FROM RaceTbl WHERE RaceID = '" & RaceID & "'"
                                        InsertQuery = "INSERT INTO RaceTbl VALUES ('" & RaceID & "'" _
                                                                    & ",'" & RaceInfo.id.Year & "'" _
                                                                    & ",'" & Month & "'" _
                                                                    & ",'" & Day & "'" _
                                                                    & ",'" & RaceInfo.id.JyoCD & "'" _
                                                                    & ",'" & RaceInfo.id.Kaiji & "'" _
                                                                    & ",'" & RaceInfo.id.Nichiji & "'" _
                                                                    & ",'" & RaceInfo.id.RaceNum & "'" _
                                                                    & ",'" & RaceName & "'" _
                                                                    & ", " & RaceInfo.Kyori _
                                                                    & ",'" & strHassoTime & "'" _
                                                                    & ",'" & RaceInfo.TrackCD & "'" _
                                                                    & ",'" & RaceInfo.TorokuTosu & "'" _
                                                                    & ",'" & RaceInfo.SyussoTosu & "'" _
                                                                    & ",'" & RaceInfo.TenkoBaba.TenkoCD & "'" _
                                                                    & ",'" & RaceInfo.TenkoBaba.SibaBabaCD & "'" _
                                                                    & ",'" & RaceInfo.TenkoBaba.DirtBabaCD & "'" _
                                                                    & ",'" & strDataType & "'" _
                                                                    & ", now(), null)"
                                        UpdateQuery = "UPDATE RaceTbl SET RaceName = '" & RaceName & "'" _
                                                        & ", Year = '" & RaceInfo.id.Year & "'" _
                                                        & ", Month = '" & Month & "'" _
                                                        & ", Day = '" & Day & "'" _
                                                        & ", Course = '" & RaceInfo.id.JyoCD & "'" _
                                                        & ", Kaiji = '" & RaceInfo.id.Kaiji & "'" _
                                                        & ", Kaiday = '" & RaceInfo.id.Nichiji & "'" _
                                                        & ", RaceNo = '" & RaceInfo.id.RaceNum & "'" _
                                                        & ", Distance = " & RaceInfo.Kyori _
                                                        & ", HassoTime = '" & strHassoTime & "'" _
                                                        & ", TrackCD = '" & RaceInfo.TrackCD & "'" _
                                                        & ", RegistHorse = '" & RaceInfo.TorokuTosu & "'" _
                                                        & ", StartHorse = '" & RaceInfo.SyussoTosu & "'" _
                                                        & ", Weather = '" & RaceInfo.TenkoBaba.TenkoCD & "'" _
                                                        & ", SibaCD = '" & RaceInfo.TenkoBaba.SibaBabaCD & "'" _
                                                        & ", DirtCD = '" & RaceInfo.TenkoBaba.DirtBabaCD & "'" _
                                                        & ", DataType = '" & strDataType & "'" _
                                                        & ", modified = now() WHERE RaceID = '" & RaceID & "'"

                                        DbConnectFlg = True
                                    Else
                                        StatusLabel.Text = String.Format("RA:Skip")
                                    End If
                                ElseIf strRecodeType = "SE" Then
                                    ' 中央競馬（確定）競馬
                                    If strDataType = "7" Then
                                        '馬毎レースのみ処理
                                        '馬毎レース構造体への展開
                                        RaceHorseInfo.SetData(strBuff)
                                        se_cnt += 1
                                        'データ表示
                                        RaceID = RaceHorseInfo.id.Year & RaceHorseInfo.id.MonthDay & RaceHorseInfo.id.JyoCD &
                                        RaceHorseInfo.id.Kaiji & RaceHorseInfo.id.Nichiji & RaceHorseInfo.id.RaceNum
                                        StatusLabel.Text = String.Format("SE:" & se_cnt & ",Race:" & RaceID & ",HorseID:" & RaceHorseInfo.KettoNum)
                                        COM.LogOut("[SE]RaceID:" & RaceID & " HorseID:" & RaceHorseInfo.KettoNum, True)
                                        'クエリー生成
                                        SelectQuery = "SELECT DataType FROM RaceDetailTbl" _
                                                                    & " WHERE RaceID = '" & RaceID & "'" _
                                                                    & " AND HorseID = '" & RaceHorseInfo.KettoNum & "'"
                                        InsertQuery = "INSERT INTO RaceDetailTbl VALUES ('" & RaceID & "'" _
                                                                    & ",'" & RaceHorseInfo.KettoNum & "'" _
                                                                    & ",'" & RaceHorseInfo.Barei & "'" _
                                                                    & ",'" & RaceHorseInfo.ChokyosiCode & "'" _
                                                                    & ",'" & RaceHorseInfo.KisyuCode & "'" _
                                                                    & ",'" & RaceHorseInfo.Wakuban & "'" _
                                                                    & ",'" & RaceHorseInfo.Umaban & "'" _
                                                                    & ",'" & RaceHorseInfo.IJyoCD & "'" _
                                                                    & ",'" & RaceHorseInfo.KakuteiJyuni & "'" _
                                                                    & ",'" & RaceHorseInfo.ChakusaCD & "'" _
                                                                    & ",'" & strDataType & "'" _
                                                                    & ", now(), null)"
                                        UpdateQuery = "UPDATE RaceDetailTbl SET HorseAge = '" & RaceHorseInfo.Barei & "'" _
                                                        & ", TrainerID = '" & RaceHorseInfo.ChokyosiCode & "'" _
                                                        & ", JockeyID = '" & RaceHorseInfo.KisyuCode & "'" _
                                                        & ", Wakuban = '" & RaceHorseInfo.Wakuban & "'" _
                                                        & ", Umaban = '" & RaceHorseInfo.Umaban & "'" _
                                                        & ", HorseStatus = '" & RaceHorseInfo.IJyoCD & "'" _
                                                        & ", RaceResult = '" & RaceHorseInfo.KakuteiJyuni & "'" _
                                                        & ", Difference = '" & RaceHorseInfo.ChakusaCD & "'" _
                                                        & ", DataType = '" & strDataType & "'" _
                                                        & ", modified = now()" _
                                                        & " WHERE RaceID = '" & RaceID & "'" _
                                                        & " AND HorseID = '" & RaceHorseInfo.KettoNum & "'"

                                        SelectQueryH = "SELECT HorseName FROM HorseTbl WHERE HorseID = " & RaceHorseInfo.KettoNum
                                        InsertQueryH = "INSERT INTO HorseTbl VALUES (" & RaceHorseInfo.KettoNum _
                                                                    & ", '" & Trim(RaceHorseInfo.Bamei) _
                                                                    & "', '', '' , now(), null)"
                                        UpdateQueryH = "UPDATE HorseTbl SET HorseName = '" & Trim(RaceHorseInfo.Bamei) _
                                                            & "', modified = now()" _
                                                            & " WHERE HorseID = " & RaceHorseInfo.KettoNum

                                        'SelectQueryC = "SELECT TrainerName FROM TrainerTbl WHERE TrainerCD = '" & RaceHorseInfo.ChokyosiCode & "'"
                                        'InsertQueryC = "INSERT INTO TrainerTbl VALUES ('" & RaceHorseInfo.ChokyosiCode _
                                        '                                        & "', '" & Trim(RaceHorseInfo.ChokyosiRyakusyo) _
                                        '                                        & "', '', '' , now(), null)"
                                        'UpdateQueryC = "UPDATE TrainerTbl SET TrainerName = '" & Trim(RaceHorseInfo.ChokyosiRyakusyo) _
                                        '                                & "', modified = now()" _
                                        '                                & " WHERE TrainerCD = '" & RaceHorseInfo.ChokyosiCode & "'"

                                        SelectQueryJ = "SELECT JockeyName FROM JockeyTbl WHERE JockeyCD = '" & RaceHorseInfo.KisyuCode & "'"
                                        InsertQueryJ = "INSERT INTO JockeyTbl VALUES ('" & RaceHorseInfo.KisyuCode _
                                                                            & "', '" & Trim(RaceHorseInfo.KisyuRyakusyo) _
                                                                            & "', '', '' , 0, now(), null)"
                                        UpdateQueryJ = "UPDATE JockeyTbl SET JockeyName = '" & Trim(RaceHorseInfo.KisyuRyakusyo) _
                                                                    & "', modified = now()" _
                                                                    & " WHERE JockeyCD = '" & RaceHorseInfo.KisyuCode & "'"

                                        DbConnectFlg = True
                                    Else
                                        StatusLabel.Text = String.Format("SE:Skip")
                                    End If
                                ElseIf strRecodeType = "HR" Then
                                    '払戻構造体への展開
                                    HaraimodoshiInfo.SetData(strBuff)
                                    'データ表示
                                    RaceID = HaraimodoshiInfo.id.Year & HaraimodoshiInfo.id.MonthDay & HaraimodoshiInfo.id.JyoCD &
                                      HaraimodoshiInfo.id.Kaiji & HaraimodoshiInfo.id.Nichiji & HaraimodoshiInfo.id.RaceNum
                                    StatusLabel.Text = String.Format("HR:" & hr_cnt & ",Race:" & RaceID)
                                    COM.LogOut("[HR]RaceID:" & RaceID, True)
                                    hr_cnt += 1
                                    'COM.LogOut("馬番:" & HaraimodoshiInfo.PayTansyo(0).Umaban & "(" & HaraimodoshiInfo.PayTansyo(0).Ninki & "):" & HaraimodoshiInfo.PayTansyo(0).Pay)
                                    'COM.LogOut("複勝１:" & HaraimodoshiInfo.PayFukusyo(0).Umaban & "(" & HaraimodoshiInfo.PayFukusyo(0).Ninki & "):" & HaraimodoshiInfo.PayFukusyo(0).Pay)
                                    'COM.LogOut("複勝２:" & HaraimodoshiInfo.PayFukusyo(1).Umaban & "(" & HaraimodoshiInfo.PayFukusyo(1).Ninki & "):" & HaraimodoshiInfo.PayFukusyo(1).Pay)
                                    'COM.LogOut("複勝３:" & HaraimodoshiInfo.PayFukusyo(2).Umaban & "(" & HaraimodoshiInfo.PayFukusyo(2).Ninki & "):" & HaraimodoshiInfo.PayFukusyo(2).Pay)
                                    'COM.LogOut("枠連１:" & Mid(HaraimodoshiInfo.PayWakuren(0).Umaban, 1, 2) & "-" & Mid(HaraimodoshiInfo.PayWakuren(0).Umaban, 3, 2) & "(" & HaraimodoshiInfo.PayWakuren(0).Ninki & "):" &
                                    '                       HaraimodoshiInfo.PayWakuren(0).Pay)
                                    'COM.LogOut("馬連１:" & Mid(HaraimodoshiInfo.PayUmaren(0).Kumi, 1, 2) & "-" & Mid(HaraimodoshiInfo.PayUmaren(0).Kumi, 3, 2) & "(" & HaraimodoshiInfo.PayUmaren(0).Ninki & "):" &
                                    '                       HaraimodoshiInfo.PayUmaren(0).Pay)
                                    'COM.LogOut("ワイド１:" & Mid(HaraimodoshiInfo.PayWide(0).Kumi, 1, 2) & "-" & Mid(HaraimodoshiInfo.PayWide(0).Kumi, 3, 2) & "(" & HaraimodoshiInfo.PayWide(0).Ninki & "):" &
                                    '                       HaraimodoshiInfo.PayWide(0).Pay)
                                    'COM.LogOut("ワイド２:" & Mid(HaraimodoshiInfo.PayWide(1).Kumi, 1, 2) & "-" & Mid(HaraimodoshiInfo.PayWide(1).Kumi, 3, 2) & "(" & HaraimodoshiInfo.PayWide(1).Ninki & "):" &
                                    '                       HaraimodoshiInfo.PayWide(1).Pay)
                                    'COM.LogOut("ワイド３:" & Mid(HaraimodoshiInfo.PayWide(2).Kumi, 1, 2) & "-" & Mid(HaraimodoshiInfo.PayWide(2).Kumi, 3, 2) & "(" & HaraimodoshiInfo.PayWide(2).Ninki & "):" &
                                    '                       HaraimodoshiInfo.PayWide(2).Pay)
                                    'COM.LogOut("馬単１:" & Mid(HaraimodoshiInfo.PayUmatan(0).Kumi, 1, 2) & "-" & Mid(HaraimodoshiInfo.PayUmatan(0).Kumi, 3, 2) & "(" & HaraimodoshiInfo.PayUmatan(0).Ninki & "):" &
                                    '                       HaraimodoshiInfo.PayUmatan(0).Pay)
                                    'COM.LogOut("３連複１:" & Mid(HaraimodoshiInfo.PaySanrenpuku(0).Kumi, 1, 2) & "-" & Mid(HaraimodoshiInfo.PaySanrenpuku(0).Kumi, 3, 2) & "-" & Mid(HaraimodoshiInfo.PaySanrenpuku(0).Kumi, 5, 2) & "(" & HaraimodoshiInfo.PaySanrenpuku(0).Ninki & "):" &
                                    '                       HaraimodoshiInfo.PaySanrenpuku(0).Pay)
                                    'COM.LogOut("３連単１:" & Mid(HaraimodoshiInfo.PaySanrentan(0).Kumi, 1, 2) & "-" & Mid(HaraimodoshiInfo.PaySanrentan(0).Kumi, 3, 2) & "-" & Mid(HaraimodoshiInfo.PaySanrentan(0).Kumi, 5, 2) & "(" & HaraimodoshiInfo.PaySanrentan(0).Ninki & "):" &
                                    '                       HaraimodoshiInfo.PaySanrentan(0).Pay)
                                    'クエリー生成
                                    SelectQueryP = "SELECT DataType FROM PayTbl" _
                                                                & " WHERE RaceID = '" & RaceID & "'"
                                    InsertQueryP = "INSERT INTO PayTbl VALUES ('" & RaceID & "'" _
                                                                & ",'" & StrToInt(HaraimodoshiInfo.PayTansyo(0).Pay) & "'" _
                                                                & ",'" & StrToInt(HaraimodoshiInfo.PayFukusyo(0).Pay) & "'" _
                                                                & ",'" & StrToInt(HaraimodoshiInfo.PayFukusyo(1).Pay) & "'" _
                                                                & ",'" & StrToInt(HaraimodoshiInfo.PayFukusyo(2).Pay) & "'" _
                                                                & ",'" & StrToInt(HaraimodoshiInfo.PayWakuren(0).Pay) & "'" _
                                                                & ",'" & StrToInt(HaraimodoshiInfo.PayUmaren(0).Pay) & "'" _
                                                                & ",'" & StrToInt(HaraimodoshiInfo.PayWide(0).Pay) & "'" _
                                                                & ",'" & StrToInt(HaraimodoshiInfo.PayWide(1).Pay) & "'" _
                                                                & ",'" & StrToInt(HaraimodoshiInfo.PayWide(2).Pay) & "'" _
                                                                & ",'" & StrToInt(HaraimodoshiInfo.PayUmatan(0).Pay) & "'" _
                                                                & ",'" & StrToInt(HaraimodoshiInfo.PaySanrenpuku(0).Pay) & "'" _
                                                                & ",'" & StrToInt(HaraimodoshiInfo.PaySanrentan(0).Pay) & "'" _
                                                                & ",'" & strDataType & "'" _
                                                                & ", now(), null)"
                                    UpdateQueryP = "UPDATE PayTbl SET Tansyo = '" & StrToInt(HaraimodoshiInfo.PayTansyo(0).Pay) & "'" _
                                                    & ", Fukusyo1 = '" & StrToInt(HaraimodoshiInfo.PayFukusyo(0).Pay) & "'" _
                                                    & ", Fukusyo2 = '" & StrToInt(HaraimodoshiInfo.PayFukusyo(1).Pay) & "'" _
                                                    & ", Fukusyo3 = '" & StrToInt(HaraimodoshiInfo.PayFukusyo(2).Pay) & "'" _
                                                    & ", Wakuren = '" & StrToInt(HaraimodoshiInfo.PayWakuren(0).Pay) & "'" _
                                                    & ", Umaren = '" & StrToInt(HaraimodoshiInfo.PayUmaren(0).Pay) & "'" _
                                                    & ", Wide1 = '" & StrToInt(HaraimodoshiInfo.PayWide(0).Pay) & "'" _
                                                    & ", Wide2 = '" & StrToInt(HaraimodoshiInfo.PayWide(1).Pay) & "'" _
                                                    & ", Wide3 = '" & StrToInt(HaraimodoshiInfo.PayWide(2).Pay) & "'" _
                                                    & ", Umatan = '" & StrToInt(HaraimodoshiInfo.PayUmatan(0).Pay) & "'" _
                                                    & ", SanrenFuku = '" & StrToInt(HaraimodoshiInfo.PaySanrenpuku(0).Pay) & "'" _
                                                    & ", Sanrentan = '" & StrToInt(HaraimodoshiInfo.PaySanrentan(0).Pay) & "'" _
                                                    & ", DataType = '" & strDataType & "'" _
                                                    & ", modified = now()" _
                                                    & " WHERE RaceID = '" & RaceID & "'"

                                    DbConnectFlg = True

                                Else
                                    '上記以外の
                                    Me.AxJVLink1.JVSkip()
                                End If
                                If DbConnectFlg = True Then
                                    'MySQLコネクション生成
                                    DB.dbConnect(connectionString)
                                    Dim rc As String

                                    Try
                                        'MySQL接続
                                        DB.dbConnectionOpen()
                                        '既存レコードの検索
                                        If Len(SelectQuery) <> 0 Then
                                            rc = DB.dbSelectDataRead(SelectQuery)
                                            COM.LogOut("Datatype=" & rc & ":" & strDataType, True)
                                            If Len(rc) > 0 Then
                                                '更新系クエリー
                                                If rc = "-1" Then
                                                    DB.dbUpdate(InsertQuery)
                                                ElseIf rc <> strDataType Then
                                                    DB.dbUpdate(UpdateQuery)
                                                End If
                                            End If
                                        End If
                                        If Len(SelectQueryH) <> 0 Then
                                            'COM.LogOut("競走馬", False)
                                            '競走馬情報登録
                                            '既存レコードの検索
                                            rc = DB.dbSelectDataRead(SelectQueryH)
                                            If Len(rc) > 0 Then
                                                If rc = "-1" Then
                                                    DB.dbUpdate(InsertQueryH)
                                                End If
                                            End If
                                        End If
                                        If Len(SelectQueryJ) <> 0 Then
                                            '騎手情報登録
                                            '既存レコードの検索
                                            rc = DB.dbSelectDataRead(SelectQueryJ)
                                            If Len(rc) > 0 Then
                                                If rc = "-1" Then
                                                    DB.dbUpdate(InsertQueryJ)
                                                End If
                                            End If
                                        End If
                                        If Len(SelectQueryP) <> 0 Then
                                            '払戻情報登録
                                            '既存レコードの検索
                                            COM.LogOut(SelectQueryP)
                                            rc = DB.dbSelectDataRead(SelectQueryP)
                                            If Len(rc) > 0 Then
                                                If rc = "-1" Then
                                                    DB.dbUpdate(InsertQueryP)
                                                End If
                                            End If
                                        End If

                                        'MySQL切断
                                        DB.dbConnectionClose()
                                    Catch e As MySqlException
                                        MsgBox("データベース接続に失敗しました。(" & DbData.DbName & ")" _
                                               & vbCrLf & "(" & e.ErrorCode & ")" & e.Message
                                               )

                                        'JVLink 終了処理
                                        lReturnCode = Me.AxJVLink1.JVClose()
                                        If lReturnCode <> 0 Then
                                            MsgBox("JVClose エラー：" & lReturnCode)
                                            Return -601
                                        End If
                                        Me.Cursor = Cursors.Default
                                        Return -600
                                    End Try
                                End If
                        End Select
                    Loop While (1)
                End If
            ElseIf lReturnCode = -1 Then
                strLastFileMstTimestamp = strMstFromTime
                COM.LogOut("最新", True)
            ElseIf lReturnCode = -504 Then
                strLastFileMstTimestamp = strMstFromTime
                COM.LogOut("メンテナンス中M：" & lReturnCode & "(" & strDataSpec & ":" & strMstFromTime & ")", True)
            Else
                strLastFileMstTimestamp = strMstFromTime
                MsgBox("JVOpen エラーM：" & lReturnCode & "(" & strDataSpec & ":" & strMstFromTime & ")")
            End If
        Catch
            Debug.WriteLine(Err.Description)
            Me.Cursor = Cursors.Default
            Return -400
        End Try
        '最終取得ファイルタイムスタンプを元に戻す
        strLastFileTimestamp = strFromTime
        'JVLink 終了処理
        lReturnCode = Me.AxJVLink1.JVClose()
        If lReturnCode <> 0 Then
            MsgBox("JVClose エラー：" & lReturnCode)
            Return -500
        End If
        Me.Cursor = Cursors.Default
        Return 0
    End Function

    Private Function GetRTRaceData(RtRaceKey As String) As Long
        Dim lReturnCode As Long
        Dim lReturn As Long
        Dim strDataSpec As String '' 引数 JVOpen:ファイル識別子
        Dim strRecodeType As String '' レコード種別
        Dim strDataType As String '' データ種別
        Const lBuffSize As Long = 150000 ''JVRead:データ格納バッファサイズ
        Const lNameSize As Integer = 256 ''JVRead:ファイル名サイズ
        Dim strBuff As String ''JVRead:データ格納バッファ
        Dim strFileName As String ''JVRead:ダウンロードファイル名
        Dim RaceInfo As JV_RA_RACE ''レース詳細情報構造体
        Dim RaceHorseInfo As JV_SE_RACE_UMA  ''馬毎レース情報構造体
        Dim strHassoTime As String '発送時刻
        Dim RaceID As String ' レースＩＤ
        Dim Month As String '開催月
        Dim Day As String '開催日
        Dim RaceName_hon As String '競争名本題
        Dim RaceName_rya As String '競争名略名
        Dim Racetype As String '
        Dim Jouken As String '
        Dim Racekigo As String '
        Dim RaceName As String '競争名
        Dim RaceCnt As Integer 'レース数

        Dim DbConnectFlg As Boolean 'DB接続フラグ

        ReDim RacesList(1)
        RaceCnt = 0
        '引数設定
        strDataSpec = "0B12" 'レコード種別（リアルタイム）

        Me.Cursor = Cursors.WaitCursor
        Dim ra_cnt As Long
        Dim se_cnt As Long
        ra_cnt = se_cnt = 0
        COM.LogOut("Key=" & RtRaceKey, False)
        strLastFileTimestamp = strFromTime
        Try
            'レースデータダウンロード
            '-----------------------------------------------------------------------------------------
            'JVLink ダウンロード処理
            COM.LogOut("dataspec = " & strDataSpec & " key = " & RtRaceKey, True)
            lReturnCode = Me.AxJVLink1.JVRTOpen(strDataSpec, RtRaceKey)
            'エラー判定
            If lReturnCode = 0 Then
                StatusLabel.Text = String.Format("リアルタイムダウンロード")
                COM.LogOut("リアルタイムダウンロード")
                'バックグラウンドでの処理を実行
                Application.DoEvents()

                Dim LogStr As String
                'Query宣言
                Dim SelectQuery As String
                Dim InsertQuery As String
                Dim UpdateQuery As String

                Do
                    LogStr = ""
                    SelectQuery = ""
                    InsertQuery = ""
                    UpdateQuery = ""
                    '構造体初期化
                    RaceInfo = New JV_RA_RACE
                    RaceHorseInfo = New JV_SE_RACE_UMA

                    'バッファ作成
                    strBuff = New String(vbNullChar, lBuffSize)
                    strFileName = New String(vbNullChar, lNameSize)
                    'JVRead で１行読み込み
                    lReturn = Me.AxJVLink1.JVRead(strBuff, lBuffSize, strFileName)
                    'COM.LogOut(lReturn & ":" & strBuff & ":" & lBuffSize & ":" & strFileName, False)
                    'リターンコードにより処理を分枝
                    DbConnectFlg = False
                    Select Case lReturn
                        Case 0 ' 全ファイル読み込み終了
                            StatusLabel.Text = String.Format("リアルタイムレースデータ終了")
                            'COM.LogOut("ファイル終わり", False)
                            Exit Do
                        Case -1 ' ファイル切り替わり
                        Case -3 ' ダウンロード中

                        Case -201 ' Init されてない
                            MsgBox("JVInit が行われていません。")
                            Exit Do
                        Case -203 ' Open されてない
                            MsgBox("JVOpen が行われていません。")
                            Exit Do
                        Case -503 ' ファイルがない
                            MsgBox(strFileName & "が存在しません。")
                            Exit Do
                        Case Is > 0 ' 正常読み込み
                            'レコード種別 ID の識別
                            strRecodeType = Mid(strBuff, 1, 2)
                            strDataType = Mid(strBuff, 3, 1)
                            COM.LogOut(strRecodeType & ":" & strDataType, False)
                            If strRecodeType = "RA" Then
                                ra_cnt += 1
                                StatusLabel.Text = String.Format("RA:" & ra_cnt)
                                'レース詳細のみ処理 'レース詳細構造体への展開
                                RaceInfo.SetData(strBuff)
                                'データ表示
                                RaceID = RaceInfo.id.Year & RaceInfo.id.MonthDay & RaceInfo.id.JyoCD &
                                      RaceInfo.id.Kaiji & RaceInfo.id.Nichiji & RaceInfo.id.RaceNum
                                RacesList(RaceCnt) = RaceID
                                RaceCnt += 1
                                ReDim Preserve RacesList(RaceCnt + 1)
                                Month = Mid(RaceInfo.id.MonthDay, 1, 2)
                                Day = Mid(RaceInfo.id.MonthDay, 3, 2)
                                'COM.LogOut("RT[RA]RaceID:" & RaceID, True)
                                LogStr = "RT[SA]RaceID:" & RaceID
                                RaceName_hon = Trim(RaceInfo.RaceInfo.Hondai) & " " & Trim(RaceInfo.RaceInfo.Fukudai)
                                '競走種別コード(2005)の1列目で変換
                                '競走記号コード(2006)の1列目で変換
                                '競走条件コード(2007)の1列目で変換
                                Racetype = objCodeConv.GetCodeName("2005", RaceInfo.JyokenInfo.SyubetuCD, "1")
                                Racekigo = objCodeConv.GetCodeName("2006", RaceInfo.JyokenInfo.KigoCD, "1")
                                Jouken = ""
                                For i As Integer = 0 To 4
                                    COM.LogOut("競争条件（" & i & "):" & RaceInfo.JyokenInfo.JyokenCD(i))
                                    If RaceInfo.JyokenInfo.JyokenCD(i) <> "000" Then
                                        Jouken = objCodeConv.GetCodeName("2007", RaceInfo.JyokenInfo.JyokenCD(i), "1")
                                        Exit For
                                    End If
                                Next i
                                RaceName_rya = Racetype & Jouken & Racekigo
                                COM.LogOut("RaceName_hon:" & RaceName_hon)
                                COM.LogOut("RaceName_rya:" & RaceName_rya)
                                If Len(Trim(RaceName_hon)) = 0 Then
                                    RaceName = RaceName_rya
                                Else
                                    RaceName = RaceName_hon
                                End If
                                'COM.LogOut("RT[RA]" & RaceID & ":" & RaceName, True)
                                StatusLabel.Text = String.Format("RA:" & ra_cnt & ",Race:" & RaceID)
                                strHassoTime = Mid(RaceInfo.HassoTime, 1, 2) & ":" & Mid(RaceInfo.HassoTime, 3, 2)
                                'クエリー生成
                                SelectQuery = "SELECT DataType FROM RaceTbl WHERE RaceID = '" & RaceID & "'"
                                InsertQuery = "INSERT INTO RaceTbl VALUES ('" & RaceID & "'" _
                                                                & ",'" & RaceInfo.id.Year & "'" _
                                                                & ",'" & Month & "'" _
                                                                & ",'" & Day & "'" _
                                                                & ",'" & RaceInfo.id.JyoCD & "'" _
                                                                & ",'" & RaceInfo.id.Kaiji & "'" _
                                                                & ",'" & RaceInfo.id.Nichiji & "'" _
                                                                & ",'" & RaceInfo.id.RaceNum & "'" _
                                                                & ",'" & RaceName & "'" _
                                                                & ", " & RaceInfo.Kyori _
                                                                & ",'" & strHassoTime & "'" _
                                                                & ",'" & RaceInfo.TrackCD & "'" _
                                                                & ",'" & RaceInfo.TorokuTosu & "'" _
                                                                & ",'" & RaceInfo.SyussoTosu & "'" _
                                                                & ",'" & RaceInfo.TenkoBaba.TenkoCD & "'" _
                                                                & ",'" & RaceInfo.TenkoBaba.SibaBabaCD & "'" _
                                                                & ",'" & RaceInfo.TenkoBaba.DirtBabaCD & "'" _
                                                                & ",'" & strDataType & "'" _
                                                                & ", now(), null)"
                                UpdateQuery = "UPDATE RaceTbl SET RaceName = '" & RaceName & "'" _
                                                    & ", Year = '" & RaceInfo.id.Year & "'" _
                                                    & ", Month = '" & Month & "'" _
                                                    & ", Day = '" & Day & "'" _
                                                    & ", Course = '" & RaceInfo.id.JyoCD & "'" _
                                                    & ", Kaiji = '" & RaceInfo.id.Kaiji & "'" _
                                                    & ", Kaiday = '" & RaceInfo.id.Nichiji & "'" _
                                                    & ", RaceNo = '" & RaceInfo.id.RaceNum & "'" _
                                                    & ", Distance = " & RaceInfo.Kyori _
                                                    & ", HassoTime = '" & strHassoTime & "'" _
                                                    & ", TrackCD = '" & RaceInfo.TrackCD & "'" _
                                                    & ", RegistHorse = '" & RaceInfo.TorokuTosu & "'" _
                                                    & ", StartHorse = '" & RaceInfo.SyussoTosu & "'" _
                                                    & ", Weather = '" & RaceInfo.TenkoBaba.TenkoCD & "'" _
                                                    & ", SibaCD = '" & RaceInfo.TenkoBaba.SibaBabaCD & "'" _
                                                    & ", DirtCD = '" & RaceInfo.TenkoBaba.DirtBabaCD & "'" _
                                                    & ", DataType = '" & strDataType & "'" _
                                                    & ", modified = now() WHERE RaceID = '" & RaceID & "'"

                                DbConnectFlg = True
                            ElseIf strRecodeType = "SE" Then
                                '馬毎レースのみ処理
                                '馬毎レース構造体への展開
                                RaceHorseInfo.SetData(strBuff)
                                se_cnt += 1
                                'データ表示
                                RaceID = RaceHorseInfo.id.Year & RaceHorseInfo.id.MonthDay & RaceHorseInfo.id.JyoCD &
                                          RaceHorseInfo.id.Kaiji & RaceHorseInfo.id.Nichiji & RaceHorseInfo.id.RaceNum
                                StatusLabel.Text = String.Format("SE:" & se_cnt & ",Race:" & RaceID & ",HorseID:" & RaceHorseInfo.KettoNum)
                                COM.LogOut(String.Format("SE:" & se_cnt & ",Race:" & RaceID & ",HorseID:" & RaceHorseInfo.KettoNum))
                                'COM.LogOut("RT[SE]RaceID:" & RaceID & ":" & Trim(RaceHorseInfo.Bamei), True)
                                LogStr = "RT[SE]RaceID:" & RaceID & ":" & Trim(RaceHorseInfo.Bamei)
                                'クエリー生成
                                SelectQuery = "SELECT DataType FROM RaceDetailTbl" _
                                                                       & " WHERE RaceID = '" & RaceID & "'" _
                                                                       & " AND HorseID = '" & RaceHorseInfo.KettoNum & "'"
                                InsertQuery = "INSERT INTO RaceDetailTbl VALUES ('" & RaceID & "'" _
                                                                       & ",'" & RaceHorseInfo.KettoNum & "'" _
                                                                       & ",'" & RaceHorseInfo.Barei & "'" _
                                                                       & ",'" & RaceHorseInfo.ChokyosiCode & "'" _
                                                                       & ",'" & RaceHorseInfo.KisyuCode & "'" _
                                                                       & ",'" & RaceHorseInfo.Wakuban & "'" _
                                                                       & ",'" & RaceHorseInfo.Umaban & "'" _
                                                                       & ",'" & RaceHorseInfo.IJyoCD & "'" _
                                                                       & ",'" & RaceHorseInfo.KakuteiJyuni & "'" _
                                                                       & ",'" & RaceHorseInfo.ChakusaCD & "'" _
                                                                       & ",'" & strDataType & "'" _
                                                                       & ", now(), null)"
                                UpdateQuery = "UPDATE RaceDetailTbl SET HorseAge = '" & RaceHorseInfo.Barei & "'" _
                                                            & ", TrainerID = '" & RaceHorseInfo.ChokyosiCode & "'" _
                                                            & ", JockeyID = '" & RaceHorseInfo.KisyuCode & "'" _
                                                            & ", Wakuban = '" & RaceHorseInfo.Wakuban & "'" _
                                                            & ", Umaban = '" & RaceHorseInfo.Umaban & "'" _
                                                            & ", HorseStatus = '" & RaceHorseInfo.IJyoCD & "'" _
                                                            & ", RaceResult = '" & RaceHorseInfo.KakuteiJyuni & "'" _
                                                            & ", Difference = '" & RaceHorseInfo.ChakusaCD & "'" _
                                                            & ", DataType = '" & strDataType & "'" _
                                                            & ", modified = now()" _
                                                            & " WHERE RaceID = '" & RaceID & "'" _
                                                            & " AND HorseID = '" & RaceHorseInfo.KettoNum & "'"

                                DbConnectFlg = True
                            End If

                            If DbConnectFlg = True Then
                                'MySQLコネクション生成
                                DB.dbConnect(connectionString)
                                Dim rc As String
                                Try
                                    'MySQL接続
                                    DB.dbConnectionOpen()
                                    '既存レコードの検索
                                    rc = DB.dbSelectDataRead(SelectQuery)
                                    '更新系クエリー
                                    If Len(rc) > 0 Then
                                        If rc = "-1" Then
                                            COM.LogOut("Datatype=" & rc & ":" & strDataType, True)
                                            COM.LogOut(LogStr, True)
                                            DB.dbUpdate(InsertQuery)
                                        ElseIf rc <> strDataType Then
                                            COM.LogOut("Datatype=" & rc & ":" & strDataType, True)
                                            COM.LogOut(LogStr, True)
                                            DB.dbUpdate(UpdateQuery)
                                        End If
                                    End If
                                    'MySQL切断
                                    DB.dbConnectionClose()
                                Catch e As MySqlException
                                    MsgBox("データベース接続に失敗しました。(" & DbData.DbName & ")" _
                                           & vbCrLf & "(" & e.ErrorCode & ")" & e.Message)
                                    'JVLink 終了処理
                                    lReturn = Me.AxJVLink1.JVClose()
                                    If lReturn <> 0 Then
                                        MsgBox("JVClose エラー：" & lReturn)
                                        Return -901
                                    End If
                                    Me.Cursor = Cursors.Default
                                    Return -900
                                End Try
                            End If
                    End Select
                Loop
            ElseIf lReturnCode = -1 Then
                strLastFileTimestamp = strFromTime
                Dim result As DialogResult
                result = CreateObject("WScript.Shell").Popup("最新です", 2, "")
                COM.LogOut("最新", True)
            ElseIf lReturnCode = -504 Then
                COM.LogOut("メンテナンス中R：" & lReturnCode & "(" & strDataSpec & ":" & strFromTime & ")", True)
                strLastFileTimestamp = strFromTime
            Else
                strLastFileTimestamp = strFromTime
                MsgBox("JVOpen エラーRT：" & lReturnCode & "(" & strDataSpec & ":" & RtRaceKey & ")")
            End If
        Catch
            Debug.WriteLine(Err.Description)
            Me.Cursor = Cursors.Default
            Return -700
        End Try
        'JVLink 終了処理
        lReturnCode = Me.AxJVLink1.JVClose()
        If lReturnCode <> 0 Then
            MsgBox("JVClose エラー：" & lReturnCode)
            Return -800
        End If
        Me.Cursor = Cursors.Default
        Return 0
    End Function

    Private Function GetRTOddsData(RtOddsKey As String) As Long
        Dim lReturnCode As Long
        Dim lReturn As Long
        Dim strDataSpec As String '' 引数 JVOpen:ファイル識別子
        Dim strRecodeType As String '' レコード種別
        Dim strDataType As String '' データ種別
        Const lBuffSize As Long = 150000 ''JVRead:データ格納バッファサイズ
        Const lNameSize As Integer = 256 ''JVRead:ファイル名サイズ
        Dim strBuff As String ''JVRead:データ格納バッファ
        Dim strFileName As String ''JVRead:ダウンロードファイル名
        Dim OddsSanrentan As JV_O6_ODDS_SANRENTAN  ''オッズ6（3連単）構造体
        Dim RaceID As String ' レースＩＤ
        Dim FtpEndFile As String
        Dim DbConnectFlg As Boolean 'DB接続フラグ

        '引数設定
        strDataSpec = "0B36" '３連単オッズ種別（リアルタイム）

        Me.Cursor = Cursors.WaitCursor

        Dim prmRaceID() As String
        Dim c As Integer
        Dim i As Integer

        'MySQLコネクション生成
        DB.dbConnect(connectionString)
        Dim rtc As Boolean

        Try
            'MySQL接続
            DB.dbConnectionOpen()
            'クエリー生成
            Dim SelectQuery As String
            SelectQuery = "SELECT RaceID from RaceTbl WHERE RaceID like '" & RtOddsKey & "%' AND (Datatype = '1' or Datatype = '2')"
            '既存レコードの検索
            rtc = DB.dbSelectDataRead3(SelectQuery, prmRaceID)
            c = 0
            COM.LogOut("dbSelectDataRead3:" & rtc)
            If rtc = True Then
                ReDim RacesList(prmRaceID.Count - 1)
                COM.LogOut("OddsTbl:" & prmRaceID.Count - 1)
                For i = 0 To prmRaceID.Count - 1
                    If Len(prmRaceID(i)) <> 0 Then
                        COM.LogOut("RacesList[" & c & "]:" & prmRaceID(i))
                        RacesList(c) = prmRaceID(i)
                        c += 1
                    End If
                Next i
            End If
            'MySQL切断
            DB.dbConnectionClose()
        Catch e As MySqlException
            MsgBox("データベース接続に失敗しました。(" & DbData.DbName & ")" _
                            & vbCrLf & "(" & e.ErrorCode & ")" & e.Message)
            Me.Cursor = Cursors.Default
            Return -990
        End Try
        COM.LogOut("RacesList:" & RacesList.Count)
        For i = 0 To RacesList.Count - 1
            COM.LogOut("Key=" & RacesList(i), False)
            If Len(RacesList(i)) <> 0 Then
                strLastFileTimestamp = strFromTime
                Try
                    'レースデータダウンロード
                    '-----------------------------------------------------------------------------------------
                    'JVLink ダウンロード処理
                    COM.LogOut("dataspec = " & strDataSpec & " key = " & RacesList(i), True)
                    lReturnCode = Me.AxJVLink1.JVRTOpen(strDataSpec, RacesList(i))
                    'エラー判定
                    If lReturnCode = 0 Then
                        StatusLabel.Text = String.Format("リアルタイムオッズダウンロード")
                        'COM.LogOut("リアルタイムオッズダウンロード")
                        'バックグラウンドでの処理を実行
                        Application.DoEvents()

                        Dim LogStr As String
                        'Query宣言
                        'オッズ情報
                        Dim SelectQueryO As String
                        Dim InsertQueryO As String
                        Dim UpdateQueryO As String

                        Do
                            LogStr = ""
                            SelectQueryO = ""
                            InsertQueryO = ""
                            UpdateQueryO = ""

                            '構造体初期化
                            OddsSanrentan = New JV_O6_ODDS_SANRENTAN

                            'バッファ作成
                            strBuff = New String(vbNullChar, lBuffSize)
                            strFileName = New String(vbNullChar, lNameSize)
                            'JVRead で１行読み込み
                            lReturn = Me.AxJVLink1.JVRead(strBuff, lBuffSize, strFileName)
                            'COM.LogOut(lReturn & ":" & strBuff & ":" & lBuffSize & ":" & strFileName, False)
                            'リターンコードにより処理を分枝
                            DbConnectFlg = False
                            Select Case lReturn
                                Case 0 ' 全ファイル読み込み終了
                                    StatusLabel.Text = String.Format("リアルタイムオッズデータ終了")
                                    'COM.LogOut("ファイル終わり", False)
                                    Exit Do
                                Case -1 ' ファイル切り替わり
                                Case -3 ' ダウンロード中

                                Case -201 ' Init されてない
                                    MsgBox("JVInit O が行われていません。")
                                    Exit Do
                                Case -203 ' Open されてない
                                    MsgBox("JVOpen O が行われていません。")
                                    Exit Do
                                Case -503 ' ファイルがない
                                    MsgBox(strFileName & "が存在しません。")
                                    Exit Do
                                Case Is > 0 ' 正常読み込み
                                    'レコード種別 ID の識別
                                    strRecodeType = Mid(strBuff, 1, 2)
                                    strDataType = Mid(strBuff, 3, 1)
                                    COM.LogOut(strRecodeType & ":" & strDataType, False)
                                    Dim putFilename As String
                                    If strRecodeType = "O6" Then
                                        '３連単構造体への展開
                                        OddsSanrentan.SetData(strBuff)
                                        'COM.LogOut("OddsSanrentan Full:" & strBuff)
                                        'データ表示
                                        RaceID = OddsSanrentan.id.Year & OddsSanrentan.id.MonthDay & OddsSanrentan.id.JyoCD &
                                                  OddsSanrentan.id.Kaiji & OddsSanrentan.id.Nichiji & OddsSanrentan.id.RaceNum
                                        StatusLabel.Text = String.Format("O6:" & ",Race:" & RaceID)
                                        putFilename = OddsFileOutput(RaceID, strBuff)
                                        'COM.LogOut("[01-02-03]" & OddsSanrentan.OddsSanrentanInfo(0).Odds)
                                        PutOddsFile(putFilename)
                                    End If
                            End Select
                        Loop
                    ElseIf lReturnCode = -1 Then
                        strLastFileTimestamp = strFromTime
                        Dim result As DialogResult
                        result = CreateObject("WScript.Shell").Popup("最新です", 2, "")
                        COM.LogOut("最新", True)
                    ElseIf lReturnCode = -504 Then
                        COM.LogOut("メンテナンス中R：" & lReturnCode & "(" & strDataSpec & ":" & strFromTime & ")", True)
                        strLastFileTimestamp = strFromTime
                    Else
                        strLastFileTimestamp = strFromTime
                        MsgBox("JVOpen エラーORT：" & lReturnCode & "(" & strDataSpec & ":" & RtOddsKey & ")")
                    End If
                Catch
                    Debug.WriteLine(Err.Description)
                    Me.Cursor = Cursors.Default
                    Return -790
                End Try
                'JVLink 終了処理
                lReturnCode = Me.AxJVLink1.JVClose()
                If lReturnCode <> 0 Then
                    MsgBox("JVClose エラー：" & lReturnCode)
                    Return -890
                End If
            End If
        Next i
        ' 転送ファイル終了ファイルを転送後削除
        FtpEndFile = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\JRA-VAN Data Lab.\uid_pass", "savepath", Nothing) & "\odds.end"
        If File.Exists(FtpEndFile) Then
            PutOddsFile(FtpEndFile)
            File.Delete(FtpEndFile)
        End If

        Me.Cursor = Cursors.Default
        Return 0
    End Function

    Private Function GetRTDMinigData(RtRaceKey As String) As Long
        Dim lReturnCode As Long
        Dim lReturn As Long
        Dim strDataSpec As String '' 引数 JVOpen:ファイル識別子
        Dim strRecodeType As String '' レコード種別
        Dim strDataType As String '' データ種別
        Const lBuffSize As Long = 150000 ''JVRead:データ格納バッファサイズ
        Const lNameSize As Integer = 256 ''JVRead:ファイル名サイズ
        Dim strBuff As String ''JVRead:データ格納バッファ
        Dim strFileName As String ''JVRead:ダウンロードファイル名
        Dim TimeMining As JV_DM_INFO  'タイム型データマイニング構造体
        Dim RaceID As String ' レースＩＤ

        Dim DbConnectFlg As Boolean 'DB接続フラグ

        '引数設定
        strDataSpec = "0B13" 'レコード種別（リアルタイム）
        Me.Cursor = Cursors.WaitCursor

        Dim prmRaceID() As String
        Dim c As Integer
        Dim i As Integer

        'MySQLコネクション生成
        DB.dbConnect(connectionString)
        Dim rtc As Boolean

        Try
            'MySQL接続
            DB.dbConnectionOpen()
            'クエリー生成
            Dim SelectQuery As String
            SelectQuery = "SELECT RaceID from RaceTbl WHERE RaceID like '" & RtRaceKey & "%' AND (Datatype = '1' or Datatype = '2')"
            '既存レコードの検索
            rtc = DB.dbSelectDataRead3(SelectQuery, prmRaceID)
            c = 0
            COM.LogOut("dbSelectDataRead3:" & rtc)
            If rtc = True Then
                ReDim RacesList(prmRaceID.Count - 1)
                COM.LogOut("OddsTbl:" & prmRaceID.Count - 1)
                For i = 0 To prmRaceID.Count - 1
                    If Len(prmRaceID(i)) <> 0 Then
                        COM.LogOut("RacesList[" & c & "]:" & prmRaceID(i))
                        RacesList(c) = prmRaceID(i)
                        c += 1
                    End If
                Next i
            End If
            'MySQL切断
            DB.dbConnectionClose()
        Catch e As MySqlException
            MsgBox("データベース接続に失敗しました。(" & DbData.DbName & ")" _
                            & vbCrLf & "(" & e.ErrorCode & ")" & e.Message)
            Me.Cursor = Cursors.Default
            Return -990
        End Try
        COM.LogOut("RacesList:" & RacesList.Count)
        For i = 0 To RacesList.Count - 1
            COM.LogOut("Key=" & RacesList(i), False)
            If Len(RacesList(i)) <> 0 Then
                strLastFileTimestamp = strFromTime
                Try
                    'レースデータダウンロード
                    '-----------------------------------------------------------------------------------------
                    'JVLink ダウンロード処理
                    COM.LogOut("dataspec = " & strDataSpec & " key = " & RacesList(i), True)
                    lReturnCode = Me.AxJVLink1.JVRTOpen(strDataSpec, RacesList(i))
                    'エラー判定
                    If lReturnCode = 0 Then
                        StatusLabel.Text = String.Format("リアルタイムダウンロード")
                        COM.LogOut("リアルタイムダウンロード")
                        'バックグラウンドでの処理を実行
                        Application.DoEvents()

                        Dim LogStr As String
                        'Query宣言
                        Dim SelectQuery(1) As String
                        Dim InsertQuery(1) As String
                        Dim UpdateQuery(1) As String

                        Do
                            LogStr = ""

                            '構造体初期化
                            TimeMining = New JV_DM_INFO

                            'バッファ作成
                            strBuff = New String(vbNullChar, lBuffSize)
                            strFileName = New String(vbNullChar, lNameSize)
                            'JVRead で１行読み込み
                            lReturn = Me.AxJVLink1.JVRead(strBuff, lBuffSize, strFileName)
                            'COM.LogOut(lReturn & ":" & strBuff & ":" & lBuffSize & ":" & strFileName, False)
                            'リターンコードにより処理を分枝
                            DbConnectFlg = False
                            Select Case lReturn
                                Case 0 ' 全ファイル読み込み終了
                                    StatusLabel.Text = String.Format("リアルタイムレースデータ終了")
                                    'COM.LogOut("ファイル終わり", False)
                                    Exit Do
                                Case -1 ' ファイル切り替わり
                                Case -3 ' ダウンロード中

                                Case -201 ' Init されてない
                                    MsgBox("JVInit が行われていません。")
                                    Exit Do
                                Case -203 ' Open されてない
                                    MsgBox("JVOpen が行われていません。")
                                    Exit Do
                                Case -503 ' ファイルがない
                                    MsgBox(strFileName & "が存在しません。")
                                    Exit Do
                                Case Is > 0 ' 正常読み込み
                                    'レコード種別 ID の識別
                                    strRecodeType = Mid(strBuff, 1, 2)
                                    strDataType = Mid(strBuff, 3, 1)
                                    COM.LogOut(strRecodeType & ":" & strDataType, False)
                                    If strRecodeType = "DM" Then
                                        'タイム型マイニング構造体への展開
                                        TimeMining.SetData(strBuff)
                                        'データ表示
                                        RaceID = TimeMining.id.Year & TimeMining.id.MonthDay & TimeMining.id.JyoCD &
                                                  TimeMining.id.Kaiji & TimeMining.id.Nichiji & TimeMining.id.RaceNum
                                        COM.LogOut("RaceID:" & RaceID)
                                        Dim MiningUmaban As String
                                        Dim ExpectTime As String
                                        Dim ExpectError As String
                                        ReDim SelectQuery(TimeMining.DMInfo.Count)
                                        ReDim InsertQuery(TimeMining.DMInfo.Count)
                                        ReDim UpdateQuery(TimeMining.DMInfo.Count)
                                        'COM.LogOut("DMInfo:" & TimeMining.DMInfo.Count)
                                        For ii As Integer = 0 To TimeMining.DMInfo.Count - 1
                                            If Trim(TimeMining.DMInfo(ii).Umaban) <> "" Then
                                                MiningUmaban = TimeMining.DMInfo(ii).Umaban
                                                'COM.LogOut("[" & MiningUmaban & "]")
                                                ExpectTime = Mid(TimeMining.DMInfo(ii).DMTime, 1, 1) & ":" _
                                                           & Mid(TimeMining.DMInfo(ii).DMTime, 2, 2) & "." _
                                                           & Mid(TimeMining.DMInfo(ii).DMTime, 4, 2)
                                                'COM.LogOut("[" & ExpectTime & "]")
                                                ExpectError = "-" & (Integer.Parse(TimeMining.DMInfo(ii).DMGosaP) / 100) & "_" & (Integer.Parse(TimeMining.DMInfo(ii).DMGosaM) / 100)
                                                'COM.LogOut("(" & ExpectError & ")")
                                                COM.LogOut("[" & MiningUmaban & "]:" & ExpectTime & "(" & ExpectError & ")")
                                                SelectQuery(ii) = "SELECT DataType FROM DataMiningTbl WHERE RaceID = '" & RaceID & "'" _
                                                                                   & " AND Umaban = '" & MiningUmaban & "'"
                                                InsertQuery(ii) = "INSERT INTO DataMiningTbl VALUES ('" & RaceID & "'" _
                                                                                   & ",'" & MiningUmaban & "'" _
                                                                                   & ",'" & ExpectTime & "'" _
                                                                                   & ",'" & ExpectError & "'" _
                                                                                   & ",''" _
                                                                                   & ",'" & strDataType & "'" _
                                                                                   & ", now(), null)"
                                                UpdateQuery(ii) = "UPDATE DataMiningTbl SET ExpectTime = '" & ExpectTime _
                                                                                & "',ExpectError = '" & ExpectError _
                                                                                & "',DataType = '" & strDataType _
                                                                                & "', modified = now()" _
                                                                                & " WHERE RaceID = '" & RaceID & "'" _
                                                                                & " AND Umaban = '" & MiningUmaban & "'"

                                            End If
                                        Next ii
                                        DbConnectFlg = True
                                    End If

                                    If DbConnectFlg = True Then
                                        'MySQLコネクション生成
                                        DB.dbConnect(connectionString)
                                        Dim rc As String
                                        Try
                                            COM.LogOut("Datatype=" & strDataType & " MakeTime=" & TimeMining.MakeHM.Hour & TimeMining.MakeHM.Minute, True)
                                            'MySQL接続
                                            DB.dbConnectionOpen()
                                            For ii As Integer = 0 To TimeMining.DMInfo.Count - 1
                                                If SelectQuery(ii) <> "" Then
                                                    '既存レコードの検索
                                                    rc = DB.dbSelectDataRead(SelectQuery(ii))
                                                    '更新系クエリー
                                                    If Len(rc) > 0 Then
                                                        If rc = "-1" Then
                                                            DB.dbUpdate(InsertQuery(ii))
                                                        ElseIf rc >= strDataType Then
                                                            DB.dbUpdate(UpdateQuery(ii))
                                                        End If
                                                    End If
                                                End If
                                            Next ii
                                            'MySQL切断
                                            DB.dbConnectionClose()
                                        Catch e As MySqlException
                                            MsgBox("データベース接続に失敗しました。(" & DbData.DbName & ")" _
                                                   & vbCrLf & "(" & e.ErrorCode & ")" & e.Message)
                                            'JVLink 終了処理
                                            lReturn = Me.AxJVLink1.JVClose()
                                            If lReturn <> 0 Then
                                                MsgBox("JVClose エラー：" & lReturn)
                                                Return -901
                                            End If
                                            Me.Cursor = Cursors.Default
                                            Return -900
                                        End Try
                                    End If
                            End Select
                        Loop
                    ElseIf lReturnCode = -1 Then
                        strLastFileTimestamp = strFromTime
                        Dim result As DialogResult
                        result = CreateObject("WScript.Shell").Popup("最新です", 2, "")
                        COM.LogOut("最新", True)
                    ElseIf lReturnCode = -504 Then
                        COM.LogOut("メンテナンス中R：" & lReturnCode & "(" & strDataSpec & ":" & strFromTime & ")", True)
                        strLastFileTimestamp = strFromTime
                    Else
                        strLastFileTimestamp = strFromTime
                        COM.LogOut("JVOpen エラーRT：" & lReturnCode & "(" & strDataSpec & ":" & RtRaceKey & ")", True)
                        MsgBox("JVOpen エラーRT：" & lReturnCode & "(" & strDataSpec & ":" & RtRaceKey & ")")
                    End If
                Catch
                    'JVLink 終了処理
                    lReturn = Me.AxJVLink1.JVClose()
                    If lReturn <> 0 Then
                        MsgBox("JVClose エラー：" & lReturn)
                        Return -901
                    End If
                    Me.Cursor = Cursors.Default
                    Return -700
                End Try
                'JVLink 終了処理
                lReturnCode = Me.AxJVLink1.JVClose()
                If lReturnCode <> 0 Then
                    MsgBox("JVClose エラー：" & lReturnCode)
                    Return -890
                End If
            End If
        Next i
        Me.Cursor = Cursors.Default
        Return 0
    End Function

    Private Function GetRTTMinigData(RtRaceKey As String) As Long
        Dim lReturnCode As Long
        Dim lReturn As Long
        Dim strDataSpec As String '' 引数 JVOpen:ファイル識別子
        Dim strRecodeType As String '' レコード種別
        Dim strDataType As String '' データ種別
        Const lBuffSize As Long = 150000 ''JVRead:データ格納バッファサイズ
        Const lNameSize As Integer = 256 ''JVRead:ファイル名サイズ
        Dim strBuff As String ''JVRead:データ格納バッファ
        Dim strFileName As String ''JVRead:ダウンロードファイル名
        Dim MatchMining As JV_TM_INFO  '対戦型データマイニング構造体
        Dim RaceID As String ' レースＩＤ

        Dim DbConnectFlg As Boolean 'DB接続フラグ

        '引数設定
        strDataSpec = "0B17" 'レコード種別（リアルタイム）

        Me.Cursor = Cursors.WaitCursor

        Dim prmRaceID() As String
        Dim c As Integer
        Dim i As Integer

        'MySQLコネクション生成
        DB.dbConnect(connectionString)
        Dim rtc As Boolean

        Try
            'MySQL接続
            DB.dbConnectionOpen()
            'クエリー生成
            Dim SelectQuery As String
            SelectQuery = "SELECT RaceID from RaceTbl WHERE RaceID like '" & RtRaceKey & "%' AND (Datatype = '1' or Datatype = '2')"
            '既存レコードの検索
            rtc = DB.dbSelectDataRead3(SelectQuery, prmRaceID)
            c = 0
            COM.LogOut("dbSelectDataRead3:" & rtc)
            If rtc = True Then
                ReDim RacesList(prmRaceID.Count - 1)
                COM.LogOut("Tbl:" & prmRaceID.Count - 1)
                For i = 0 To prmRaceID.Count - 1
                    If Len(prmRaceID(i)) <> 0 Then
                        COM.LogOut("RacesList[" & c & "]:" & prmRaceID(i))
                        RacesList(c) = prmRaceID(i)
                        c += 1
                    End If
                Next i
            End If
            'MySQL切断
            DB.dbConnectionClose()
        Catch e As MySqlException
            MsgBox("データベース接続に失敗しました。(" & DbData.DbName & ")" _
                            & vbCrLf & "(" & e.ErrorCode & ")" & e.Message)
            Me.Cursor = Cursors.Default
            Return -990
        End Try
        COM.LogOut("RacesList:" & RacesList.Count)
        For i = 0 To RacesList.Count - 1
            COM.LogOut("Key=" & RacesList(i), False)
            If Len(RacesList(i)) <> 0 Then
                strLastFileTimestamp = strFromTime
                Try
                    'レースデータダウンロード
                    '-----------------------------------------------------------------------------------------
                    'JVLink ダウンロード処理
                    COM.LogOut("dataspec = " & strDataSpec & " key = " & RacesList(i), True)
                    lReturnCode = Me.AxJVLink1.JVRTOpen(strDataSpec, RacesList(i))
                    'エラー判定
                    If lReturnCode = 0 Then
                        StatusLabel.Text = String.Format("リアルタイムダウンロード")
                        COM.LogOut("リアルタイムダウンロード")
                        'バックグラウンドでの処理を実行
                        Application.DoEvents()

                        Dim LogStr As String
                        'Query宣言
                        Dim SelectQuery(1) As String
                        Dim InsertQuery(1) As String
                        Dim UpdateQuery(1) As String

                        Do
                            LogStr = ""

                            '構造体初期化
                            MatchMining = New JV_TM_INFO

                            'バッファ作成
                            strBuff = New String(vbNullChar, lBuffSize)
                            strFileName = New String(vbNullChar, lNameSize)
                            'JVRead で１行読み込み
                            lReturn = Me.AxJVLink1.JVRead(strBuff, lBuffSize, strFileName)
                            'COM.LogOut(lReturn & ":" & strBuff & ":" & lBuffSize & ":" & strFileName, False)
                            'リターンコードにより処理を分枝
                            DbConnectFlg = False
                            Select Case lReturn
                                Case 0 ' 全ファイル読み込み終了
                                    StatusLabel.Text = String.Format("リアルタイムレースデータ終了")
                                    'COM.LogOut("ファイル終わり", False)
                                    Exit Do
                                Case -1 ' ファイル切り替わり
                                Case -3 ' ダウンロード中

                                Case -201 ' Init されてない
                                    MsgBox("JVInit が行われていません。")
                                    Exit Do
                                Case -203 ' Open されてない
                                    MsgBox("JVOpen が行われていません。")
                                    Exit Do
                                Case -503 ' ファイルがない
                                    MsgBox(strFileName & "が存在しません。")
                                    Exit Do
                                Case Is > 0 ' 正常読み込み
                                    'レコード種別 ID の識別
                                    strRecodeType = Mid(strBuff, 1, 2)
                                    strDataType = Mid(strBuff, 3, 1)
                                    COM.LogOut(strRecodeType & ":" & strDataType, False)
                                    If strRecodeType = "TM" Then
                                        '対戦型マイニング構造体への展開
                                        MatchMining.SetData(strBuff)
                                        'データ表示
                                        RaceID = MatchMining.id.Year & MatchMining.id.MonthDay & MatchMining.id.JyoCD &
                                          MatchMining.id.Kaiji & MatchMining.id.Nichiji & MatchMining.id.RaceNum
                                        COM.LogOut("RaceID:" & RaceID)
                                        Dim MiningUmaban As String
                                        Dim ExpectScore As String
                                        ReDim SelectQuery(MatchMining.TMInfo.Count)
                                        ReDim InsertQuery(MatchMining.TMInfo.Count)
                                        ReDim UpdateQuery(MatchMining.TMInfo.Count)
                                        For ii As Integer = 0 To MatchMining.TMInfo.Count - 1
                                            If Trim(MatchMining.TMInfo(ii).Umaban) <> "" Then
                                                MiningUmaban = MatchMining.TMInfo(ii).Umaban
                                                ExpectScore = Integer.Parse(MatchMining.TMInfo(ii).TMScore) / 10
                                                COM.LogOut("[" & MiningUmaban & "]:" & ExpectScore)
                                                SelectQuery(ii) = "SELECT DataType FROM DataMiningTbl WHERE RaceID = '" & RaceID & "'" _
                                                                                   & " AND Umaban = '" & MiningUmaban & "'"
                                                InsertQuery(ii) = "INSERT INTO DataMiningTbl VALUES ('" & RaceID & "'" _
                                                                                   & ",'" & MiningUmaban & "'" _
                                                                                   & ",''" _
                                                                                   & ",''" _
                                                                                   & ",'" & ExpectScore & "'" _
                                                                                   & ",'" & strDataType & "'" _
                                                                                   & ", now(), null)"
                                                UpdateQuery(ii) = "UPDATE DataMiningTbl SET ExpectScore = '" & ExpectScore _
                                                                                & "',DataType = '" & strDataType _
                                                                                & "',modified = now()" _
                                                                                & " WHERE RaceID = '" & RaceID & "'" _
                                                                                & " AND Umaban = '" & MiningUmaban & "'"
                                            End If
                                        Next ii
                                        DbConnectFlg = True
                                    End If

                                    If DbConnectFlg = True Then
                                        'MySQLコネクション生成
                                        DB.dbConnect(connectionString)
                                        Dim rc As String
                                        Try
                                            COM.LogOut("Datatype=" & strDataType & " MakeTime=" & MatchMining.MakeHM.Hour & MatchMining.MakeHM.Minute, True)
                                            'MySQL接続
                                            DB.dbConnectionOpen()
                                            For ii As Integer = 0 To MatchMining.TMInfo.Count - 1
                                                If SelectQuery(ii) <> "" Then
                                                    '既存レコードの検索
                                                    rc = DB.dbSelectDataRead(SelectQuery(ii))
                                                    '更新系クエリー
                                                    If Len(rc) > 0 Then
                                                        If rc = "-1" Then
                                                            DB.dbUpdate(InsertQuery(ii))
                                                        ElseIf rc >= strDataType Then
                                                            DB.dbUpdate(UpdateQuery(ii))
                                                        End If
                                                    End If
                                                End If
                                            Next ii
                                            'MySQL切断
                                            DB.dbConnectionClose()
                                        Catch e As MySqlException
                                            MsgBox("データベース接続に失敗しました。(" & DbData.DbName & ")" _
                                           & vbCrLf & "(" & e.ErrorCode & ")" & e.Message)
                                            'JVLink 終了処理
                                            lReturn = Me.AxJVLink1.JVClose()
                                            If lReturn <> 0 Then
                                                MsgBox("JVClose エラー：" & lReturn)
                                                Return -901
                                            End If
                                            Me.Cursor = Cursors.Default
                                            Return -900
                                        End Try
                                    End If
                            End Select
                        Loop
                    ElseIf lReturnCode = -1 Then
                        strLastFileTimestamp = strFromTime
                        Dim result As DialogResult
                        result = CreateObject("WScript.Shell").Popup("最新です", 2, "")
                        COM.LogOut("最新", True)
                    ElseIf lReturnCode = -504 Then
                        COM.LogOut("メンテナンス中R：" & lReturnCode & "(" & strDataSpec & ":" & strFromTime & ")", True)
                        strLastFileTimestamp = strFromTime
                    Else
                        strLastFileTimestamp = strFromTime
                        COM.LogOut("JVOpen エラーRT：" & lReturnCode & "(" & strDataSpec & ":" & RacesList(i) & ")", True)
                        MsgBox("JVOpen エラーRT：" & lReturnCode & "(" & strDataSpec & ":" & RacesList(i) & ")")
                    End If
                Catch
                    'JVLink 終了処理
                    lReturn = Me.AxJVLink1.JVClose()
                    If lReturn <> 0 Then
                        MsgBox("JVClose エラー：" & lReturn)
                        Return -901
                    End If
                    Me.Cursor = Cursors.Default
                    Return -790
                End Try
                'JVLink 終了処理
                lReturnCode = Me.AxJVLink1.JVClose()
                If lReturnCode <> 0 Then
                    MsgBox("JVClose エラー：" & lReturnCode)
                    Return -700
                End If
            End If
        Next i

        Me.Cursor = Cursors.Default
        Return 0
    End Function
    Private Function GetScheduleData() As Long
        Dim lReturnCode As Long
        Dim strDataSpec As String '' 引数 JVOpen:ファイル識別子
        Dim lOption As Long '' 引数 JVOpen:オプション
        Dim strScheduleFromTime As String  '' 引数 JVOpen:データ提供日付
        Dim strLastScheduleTimestamp As String
        Dim lReadCount As Long '' JVLink 戻り値
        Dim lDownloadCount As Long '' JVOpen:総ダウンロードファイル数
        Dim strRecodeType As String '' レコード種別
        Dim strDataType As String '' データ種別
        Dim RecodeCnt As Integer 'レコード数
        Const lBuffSize As Long = 110000 ''JVRead:データ格納バッファサイズ
        Const lNameSize As Integer = 256 ''JVRead:ファイル名サイズ
        Dim strBuff As String ''JVRead:データ格納バッファ
        Dim strFileName As String ''JVRead:ダウンロードファイル名
        Dim YsSchedule As JV_YS_SCHEDULE ''開催スケジュール構造体
        Dim DbConnectFlg As Boolean 'DB接続フラグ
        RecodeCnt = 0 'レコード数

        '引数設定
        strDataSpec = "YSCH" 'レコード種別（蓄積情報）
        lOption = 1
        Dim Today As DateTime = DateTime.Now
        strScheduleFromTime = Format(Today, "yyyy") & "0101000000"
        COM.LogOut("Opt:" & lOption & "  FromTime:" & strScheduleFromTime, True)
        Me.Cursor = Cursors.WaitCursor
        Dim i As Long
        Dim um_cnt As Long
        Dim ch_cnt As Long
        Dim ks_cnt As Long
        i = um_cnt = ch_cnt = ks_cnt = 0

        Try
            'レースデータダウンロード
            '-----------------------------------------------------------------------------------------
            'JVLink ダウンロード処理
            lReturnCode = Me.AxJVLink1.JVOpen(strDataSpec, strScheduleFromTime, lOption, lReadCount, lDownloadCount, strLastScheduleTimestamp)
            'エラー判定
            If lReturnCode = 0 Then
                StatusLabel.Text = String.Format("ダウンロードファイル数 :" & lDownloadCount)
                'Lesson 3追加
                If lReadCount > 0 Then
                    Do
                        'バックグラウンドでの処理を実行
                        Application.DoEvents()

                        'Query宣言
                        Dim SelectQuery As String
                        Dim InsertQuery As String
                        Dim UpdateQuery As String

                        SelectQuery = ""
                        InsertQuery = ""
                        UpdateQuery = ""

                        '構造体初期化
                        YsSchedule = New JV_YS_SCHEDULE

                        'バッファ作成
                        strBuff = New String(vbNullChar, lBuffSize)
                        strFileName = New String(vbNullChar, lNameSize)
                        'JVRead で１行読み込み
                        lReturnCode = Me.AxJVLink1.JVRead(strBuff, lBuffSize, strFileName)
                        'リターンコードにより処理を分枝
                        DbConnectFlg = False
                        Select Case lReturnCode
                            Case 0 ' 全ファイル読み込み終了
                                StatusLabel.Text = String.Format("スケジュールデータ終了")
                                Exit Do
                            Case -1 ' ファイル切り替わり
                            Case -3 ' ダウンロード中
                            Case -201 ' Init されてない
                                MsgBox("JVInit が行われていません。")
                                Exit Do
                            Case -203 ' Open されてない
                                MsgBox("JVOpen が行われていません。")
                                Exit Do
                            Case -503 ' ファイルがない
                                MsgBox(strFileName & "が存在しません。")
                                Exit Do
                            Case Is > 0 ' 正常読み込み
                                'レコード種別 ID の識別
                                strRecodeType = Mid(strBuff, 1, 2)
                                strDataType = Mid(strBuff, 3, 1)
                                COM.LogOut(strRecodeType & ":" & strDataType, False)
                                'レコード種別 ID の識別
                                If strRecodeType = "YS" Then
                                    '開催スケジュール構造体への展開
                                    YsSchedule.SetData(strBuff)
                                    COM.LogOut("YsSchedule Full:" & strBuff)
                                    COM.LogOut("Schedule Make:" & YsSchedule.head.MakeDate.Year & YsSchedule.head.MakeDate.Month & YsSchedule.head.MakeDate.Day, True)
                                    RecodeCnt += 1
                                    'データ表示
                                    COM.LogOut("Schedule Date:" & YsSchedule.id.Year & YsSchedule.id.MonthDay & " 場:" & YsSchedule.id.JyoCD, True)
                                    Dim Jyusho As String = "0"
                                    For ii As Integer = 0 To 2
                                        If YsSchedule.JyusyoInfo(ii).TokuNum <> "0000" Then
                                            COM.LogOut("Schedule 重" & YsSchedule.JyusyoInfo(ii).GradeCD & ":[" & YsSchedule.JyusyoInfo(ii).TokuNum & "]" & Trim(YsSchedule.JyusyoInfo(ii).Hondai), True)
                                            Jyusho = "1"
                                        Else
                                            Exit For
                                        End If
                                    Next ii
                                    'クエリー生成
                                    SelectQuery = "SELECT DataType FROM ScheduleTbl" _
                                                                       & " WHERE Year = '" & YsSchedule.id.Year & "'" _
                                                                       & " AND MonthDay = '" & YsSchedule.id.MonthDay & "'" _
                                                                       & " AND Keibajyo = '" & YsSchedule.id.JyoCD & "'"
                                    InsertQuery = "INSERT INTO ScheduleTbl VALUES ('" & YsSchedule.id.Year & "'" _
                                                                       & ",'" & YsSchedule.id.MonthDay & "'" _
                                                                       & ",'" & YsSchedule.id.JyoCD & "'" _
                                                                       & ",'" & YsSchedule.id.Kaiji & "'" _
                                                                       & ",'" & YsSchedule.id.Nichiji & "'" _
                                                                       & ",'" & Jyusho & "'" _
                                                                       & ",'" & strDataType & "'" _
                                                                       & ", now(), null)"
                                    UpdateQuery = "UPDATE ScheduleTbl SET DataType = '" & strDataType & "'" _
                                                                       & " WHERE Year = '" & YsSchedule.id.Year & "'" _
                                                                       & " AND MonthDay = '" & YsSchedule.id.MonthDay & "'" _
                                                                       & " AND Keibajyo = '" & YsSchedule.id.JyoCD & "'"
                                    DbConnectFlg = True
                                Else
                                    '上記以外の
                                    Me.AxJVLink1.JVSkip()
                                End If
                                If DbConnectFlg = True Then
                                    'MySQLコネクション生成
                                    'Dim connectionString = "server=139.162.88.225;database=HorseRacingTipsDB;user id=HRT;password=HorseRacingTips"
                                    DB.dbConnect(connectionString)
                                    Dim rc As String

                                    Try
                                        'MySQL接続
                                        DB.dbConnectionOpen()
                                        '既存レコードの検索
                                        rc = DB.dbSelectDataRead(SelectQuery)
                                        COM.LogOut("Datatype=" & rc & ":" & strDataType, True)
                                        If Len(rc) > 0 Then
                                            '更新系クエリー
                                            If rc = "-1" Then
                                                DB.dbUpdate(InsertQuery)
                                            ElseIf rc <> strDataType Then
                                                DB.dbUpdate(UpdateQuery)
                                            End If
                                        End If

                                        'MySQL切断
                                        DB.dbConnectionClose()
                                    Catch e As MySqlException
                                        MsgBox("データベース接続に失敗しました。(" & DbData.DbName & ")" _
                                               & vbCrLf & "(" & e.ErrorCode & ")" & e.Message
                                               )

                                        'JVLink 終了処理
                                        lReturnCode = Me.AxJVLink1.JVClose()
                                        If lReturnCode <> 0 Then
                                            MsgBox("JVClose エラー：" & lReturnCode)
                                            Return -601
                                        End If
                                        Me.Cursor = Cursors.Default
                                        Return -600
                                    End Try
                                End If
                        End Select
                    Loop While (1)
                End If
            ElseIf lReturnCode = -1 Then
                strLastScheduleTimestamp = strScheduleFromTime
                COM.LogOut("最新", True)
            ElseIf lReturnCode = -504 Then
                strLastScheduleTimestamp = strScheduleFromTime
                COM.LogOut("メンテナンス中M：" & lReturnCode & "(" & strDataSpec & ":" & strScheduleFromTime & ")", True)
            Else
                strLastScheduleTimestamp = strScheduleFromTime
                COM.LogOut("JVOpen エラーM：" & lReturnCode & "(" & strDataSpec & ":" & strScheduleFromTime & ")", True)
                MsgBox("JVOpen エラーM：" & lReturnCode & "(" & strDataSpec & ":" & strScheduleFromTime & ")")
            End If
        Catch
            Debug.WriteLine(Err.Description)
            Me.Cursor = Cursors.Default
            Return -400
        End Try
        '最終取得ファイルタイムスタンプを元に戻す
        strLastFileTimestamp = strFromTime
        'JVLink 終了処理
        lReturnCode = Me.AxJVLink1.JVClose()
        If lReturnCode <> 0 Then
            MsgBox("JVClose エラー：" & lReturnCode)
            Return -500
        End If
        Me.Cursor = Cursors.Default
        Return 0
    End Function
    Private Function StrToInt(ByVal str As String) As Integer
        Dim IntOdds As Integer
        If Integer.TryParse(str, IntOdds) Then
            Return IntOdds
        Else
            Return 0
        End If
    End Function
    Private Function oddsConvert(ByVal strOdds As String) As String
        Dim IntOdds As Integer
        If Integer.TryParse(strOdds, IntOdds) Then
            Return IntOdds / 10 & ""
        Else
            Return strOdds & ""
        End If
    End Function

    Private Function AllButtonControl(ByVal cont As Boolean) As Boolean
        If cont = False Then
            btnJVInitSetting.Enabled = False
            btnDBSetting.Enabled = False
            JVDataGetStart.Enabled = False
            JVDataGetMst.Enabled = False
            JVDataGetStop.Enabled = False
            ProgramEnd.Enabled = False
            JVDataGetPast.Enabled = False
        Else
            btnJVInitSetting.Enabled = True
            btnDBSetting.Enabled = True
            JVDataGetStart.Enabled = True
            JVDataGetMst.Enabled = True
            JVDataGetStop.Enabled = True
            ProgramEnd.Enabled = True
            JVDataGetPast.Enabled = True
        End If
        Return True
    End Function
    Private Shared Sub AddText(ByVal fs As FileStream, ByVal value As String)
        Dim info As Byte() = New UTF8Encoding(True).GetBytes(value)
        fs.Write(info, 0, info.Length)
    End Sub

    'Private Sub JVDataGetMst_GotFocus(sender As Object, e As EventArgs) Handles JVDataGetMst.GotFocus
    '    Help.ShowPopup(Me, "マスタデータ取得は現時点では不要です", Control.MousePosition)
    'End Sub

    ' フォルダ内のファイルを一括削除
    Private Function DeleteFolder(ByVal strDesFolderName As String) As Boolean
        Dim DeleteDate As Date
        Dim FileDate As Date
        '戻り値初期化
        DeleteFolder = False

        'COM.LogOut(strDesFolderName, False)
        DeleteDate = DateAdd("d", -1, Today)

        Try
            '指定フォルダ内の全ファイルを取得
            Dim arrFiles() As String = System.IO.Directory.GetFiles(strDesFolderName)

            '全ファイル削除
            COM.LogOut(DeleteDate, False)
            For Each strFile As String In arrFiles
                FileDate = System.IO.File.GetLastWriteTime(strFile)
                COM.LogOut(strFile & ”：" & FileDate, False)
                If DeleteDate > FileDate Then
                    'COM.LogOut(strFile, False)
                    System.IO.File.Delete(strFile)
                End If
            Next

            '正常終了
            Return True

        Catch ex As Exception
            COM.LogOut(ex.Message, True)
        End Try
    End Function
    Private Function OddsFileOutput(ByVal Filename As String, ByVal txtString As String) As String
        Dim outputFolder As String
        Dim outputFile As String
        Dim FtpEndFile As String
        Dim fs As FileStream

        Dim readValue = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\JRA-VAN Data Lab.\uid_pass", "savepath", Nothing)
        outputFolder = readValue & "\cache"
        outputFile = outputFolder & "\ODDS06" & Filename & ".dat"
        Dim utf8Enc As Encoding = Encoding.GetEncoding("UTF-8")
        Using writer As StreamWriter = New StreamWriter(outputFile, False, utf8Enc)
            'COM.LogOut("File:" & outputFile)
            'COM.LogOut("STR:" & txtString)
            writer.WriteLine(txtString)
        End Using

        ' 転送ファイル終了ファイルを作成
        FtpEndFile = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\JRA-VAN Data Lab.\uid_pass", "savepath", Nothing) & "\odds.end"
        If File.Exists(FtpEndFile) = False Then
            fs = File.Create(FtpEndFile)
            fs.Dispose()
        End If

        Return outputFile
    End Function

    Private Function PutOddsFile(ByVal Filename As String) As Boolean
        Dim UploadFile As String
        'アップロードするファイル
        UploadFile = Path.GetFileName(Filename)

        'COM.LogOut("Read File is " & Filename)
        'COM.LogOut("Upload File is " & UploadFile)

        'COM.LogOut("Sftp Connect is " & "139.162.88.225")

        'SFTP接続
        Dim client As SftpClient = New SftpClient("139.162.88.225", "root", "lKR6Vpy%a3n$toV")
        client.Connect()

        '転送
        Using stream As Stream = File.OpenRead(Filename)
            client.UploadFile(stream, "/tmp/ftp/" & UploadFile)
        End Using

        '切断
        client.Disconnect()

        Return True
    End Function
End Class