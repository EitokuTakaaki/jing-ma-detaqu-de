﻿Imports MySql.Data.MySqlClient

Public Class MySQLComCls
    'MySQLコネクション識別子
    Private Property m_connection As MySqlConnection
    Private COM As ComFunction = New ComFunction

    ' 接続情報の作成
    Public Sub dbConnect(strConn As String)
        m_connection = New MySqlConnection(strConn)
    End Sub

    ' 接続
    Public Sub dbConnectionOpen()
        If Not m_connection Is Nothing Then
            m_connection.Open()
        End If
    End Sub

    ' 切断
    Public Sub dbConnectionClose()
        If Not m_connection Is Nothing Then
            m_connection.Close()
        End If
    End Sub

    ' 検索
    Public Function dbSelect(query As String) As Boolean
        Dim Cmd = New MySqlCommand(query, m_connection)
        Dim dr As MySqlDataReader
        Dim RtnCd As Boolean
        'COM.LogOut(query, False)
        dr = Cmd.ExecuteReader()
        If dr.HasRows = True Then
            RtnCd = True
        Else
            RtnCd = False
        End If
        dr.Close()
        Return RtnCd
    End Function

    Public Function dbSelectDataRead(query As String) As String
        Dim Cmd = New MySqlCommand(query, m_connection)
        Dim dr As MySqlDataReader
        Dim RtnCd As String
        'COM.LogOut(query, False)
        dr = Cmd.ExecuteReader()
        RtnCd = ""
        If dr.HasRows = True Then
            While dr.Read()
                RtnCd = dr.GetString(0)
            End While
            'COM.LogOut("Found " & RtnCd)
        Else
            RtnCd = "-1"
            'COM.LogOut("Not Found " & RtnCd)
        End If
        dr.Close()
        Return RtnCd
    End Function

    Public Function dbSelectDataRead2(query As String, ByRef prm1 As String, ByRef prm2 As String) As Boolean
        Dim Cmd = New MySqlCommand(query, m_connection)
        Dim dr As MySqlDataReader
        Dim RtnCd As Boolean
        'COM.LogOut(query, False)
        dr = Cmd.ExecuteReader()
        RtnCd = False
        If dr.HasRows = True Then
            While dr.Read()
                prm1 = dr.GetString(0)
                prm2 = dr.GetString(1)
            End While
            'COM.LogOut("Found " & "Type:" & d_type & " Time:" & h_time)
            RtnCd = True
        End If
        dr.Close()
        Return RtnCd
    End Function
    Public Function dbSelectDataRead3(query As String, ByRef prm1() As String) As Boolean
        Dim Cmd = New MySqlCommand(query, m_connection)
        Dim dr As MySqlDataReader
        Dim RtnCd As Boolean
        Dim cnt As Integer
        'COM.LogOut(query, False)
        dr = Cmd.ExecuteReader()
        cnt = 0
        RtnCd = False
        If dr.HasRows = True Then
            While dr.Read()
                'COM.LogOut(dr.GetValue(0), False)
                ReDim Preserve prm1(cnt + 1)
                prm1(cnt) = dr.GetValue(0)
                cnt += 1
            End While
            'COM.LogOut("Found " & "Type:" & d_type & " Time:" & h_time)
            RtnCd = True
        End If
        dr.Close()
        Return RtnCd
    End Function

    ' 更新
    Public Function dbUpdate(query As String) As Integer
        Dim Cmd = New MySqlCommand(query, m_connection)
        Dim RtnCd As Integer = 0
        'COM.LogOut(query, False)
        RtnCd = Cmd.ExecuteNonQuery()
        Return RtnCd
    End Function

End Class
