﻿Imports System.IO
Imports System.Text

Public Class frmDBSet
    Private COM As ComFunction     '共通関数

    Private Sub frmDBSet_Load(sender As Object, e As EventArgs) Handles Me.Load
        COM = New ComFunction
    End Sub

    Private Sub btnRegist_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRegist.Click
        'INIフォルダ
        Dim tempPath = Environment.GetEnvironmentVariable("temp")
        Dim strPath = tempPath & "\..\BlueMarlin"
        COM.LogOut(strPath, False)
        If System.IO.Directory.Exists(strPath) = False Then
            System.IO.Directory.CreateDirectory(strPath)
        End If
        'ファイル存在チェック
        Dim boolFile_Exists As Boolean
        boolFile_Exists = System.IO.File.Exists(strPath & "\horseracing.ini")
        If boolFile_Exists = False Then
            ' ファイルに対するリファレンスを作成する
            Dim fi As FileInfo = New FileInfo(strPath & "\horseracing.ini")
            ' 実際にファイルを作成する
            Using fs As FileStream = fi.Create()
                AddText(fs, "[DATABASE]" & vbCrLf)
                AddText(fs, "SERVER_NAME=" & txtDBServer.Text & vbCrLf)
                AddText(fs, "DATABASE_NAME=" & txtDBName.Text & vbCrLf)
                AddText(fs, "DATABASE_USER=" & txtDBUser.Text & vbCrLf)
                AddText(fs, "DATABASE_PASSWORD=" & txtDBPassword.Text & vbCrLf)
                ' ファイルストリームを閉じて、変更を確定させる
                ' 呼ばなくても Using を抜けた時点で Dispose メソッドが呼び出される
                fs.Close()
            End Using
        Else
            'INIファイルクラスの生成
            Dim Ini As New clsIniConfig(strPath & "\horseracing.ini")
            'INIファイルへの書込み
            Ini.WriteProfileString("DATABASE", "SERVER_NAME", txtDBServer.Text)
            Ini.WriteProfileString("DATABASE", "DATABASE_NAME", txtDBName.Text)
            Ini.WriteProfileString("DATABASE", "DATABASE_USER", txtDBUser.Text)
            Ini.WriteProfileString("DATABASE", "DATABASE_PASSWORD", txtDBPassword.Text)
        End If
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        'キャンセルボタンが押された時はDialogResult.Cancelを設定する。
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        'ShowDialog()で表示されているので閉じないといけない
        Me.Close()
    End Sub

    Private Sub ckbPasswdDisp_CheckedChanged(sender As Object, e As EventArgs) Handles ckbPasswdDisp.CheckedChanged
        Select Case DirectCast(sender, CheckBox).CheckState
            Case CheckState.Checked
                txtDBPassword.PasswordChar = CChar(vbNullString)
            Case CheckState.Unchecked
                txtDBPassword.PasswordChar = CChar("*")
        End Select
    End Sub
    Private Shared Sub AddText(ByVal fs As FileStream, ByVal value As String)
        Dim info As Byte() = New UTF8Encoding(True).GetBytes(value)
        fs.Write(info, 0, info.Length)
    End Sub
End Class