﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class HorseRacingTips
    Inherits System.Windows.Forms.Form

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナーで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
    'Windows フォーム デザイナーを使用して変更できます。  
    'コード エディターを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(HorseRacingTips))
        Me.btnJVInitSetting = New System.Windows.Forms.Button()
        Me.AxJVLink1 = New AxJVDTLabLib.AxJVLink()
        Me.StatusStrip = New System.Windows.Forms.StatusStrip()
        Me.StatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me.JVDataGetStart = New System.Windows.Forms.Button()
        Me.ProgramEnd = New System.Windows.Forms.Button()
        Me.HorseRacingTipsIcon = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.終了ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.JVDataGetStop = New System.Windows.Forms.Button()
        Me.GetTimer = New System.Windows.Forms.Timer(Me.components)
        Me.btnDBSetting = New System.Windows.Forms.Button()
        Me.lblCountDown = New System.Windows.Forms.Label()
        Me.JVDataGetMst = New System.Windows.Forms.Button()
        Me.lblVersion = New System.Windows.Forms.Label()
        Me.JVDataGetPast = New System.Windows.Forms.Button()
        CType(Me.AxJVLink1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip.SuspendLayout()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnJVInitSetting
        '
        Me.btnJVInitSetting.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.btnJVInitSetting.Font = New System.Drawing.Font("MS UI Gothic", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.btnJVInitSetting.Location = New System.Drawing.Point(564, 12)
        Me.btnJVInitSetting.Name = "btnJVInitSetting"
        Me.btnJVInitSetting.Size = New System.Drawing.Size(166, 72)
        Me.btnJVInitSetting.TabIndex = 2
        Me.btnJVInitSetting.Text = "JVInit設定"
        Me.btnJVInitSetting.UseVisualStyleBackColor = False
        '
        'AxJVLink1
        '
        Me.AxJVLink1.Enabled = True
        Me.AxJVLink1.Location = New System.Drawing.Point(47, 12)
        Me.AxJVLink1.Name = "AxJVLink1"
        Me.AxJVLink1.OcxState = CType(resources.GetObject("AxJVLink1.OcxState"), System.Windows.Forms.AxHost.State)
        Me.AxJVLink1.Size = New System.Drawing.Size(192, 192)
        Me.AxJVLink1.TabIndex = 1
        '
        'StatusStrip
        '
        Me.StatusStrip.Font = New System.Drawing.Font("Yu Gothic UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.StatusLabel})
        Me.StatusStrip.Location = New System.Drawing.Point(0, 191)
        Me.StatusStrip.Name = "StatusStrip"
        Me.StatusStrip.Size = New System.Drawing.Size(922, 22)
        Me.StatusStrip.TabIndex = 2
        Me.StatusStrip.Text = "StatusStrip1"
        '
        'StatusLabel
        '
        Me.StatusLabel.Name = "StatusLabel"
        Me.StatusLabel.Size = New System.Drawing.Size(0, 17)
        '
        'JVDataGetStart
        '
        Me.JVDataGetStart.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.JVDataGetStart.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.JVDataGetStart.Font = New System.Drawing.Font("MS UI Gothic", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.JVDataGetStart.Location = New System.Drawing.Point(26, 12)
        Me.JVDataGetStart.Name = "JVDataGetStart"
        Me.JVDataGetStart.Size = New System.Drawing.Size(249, 130)
        Me.JVDataGetStart.TabIndex = 0
        Me.JVDataGetStart.Text = "取得開始"
        Me.JVDataGetStart.UseVisualStyleBackColor = False
        '
        'ProgramEnd
        '
        Me.ProgramEnd.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ProgramEnd.Font = New System.Drawing.Font("MS UI Gothic", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.ProgramEnd.Location = New System.Drawing.Point(821, 99)
        Me.ProgramEnd.Name = "ProgramEnd"
        Me.ProgramEnd.Size = New System.Drawing.Size(81, 68)
        Me.ProgramEnd.TabIndex = 5
        Me.ProgramEnd.Text = "終了"
        Me.ProgramEnd.UseVisualStyleBackColor = False
        '
        'HorseRacingTipsIcon
        '
        Me.HorseRacingTipsIcon.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info
        Me.HorseRacingTipsIcon.ContextMenuStrip = Me.ContextMenuStrip1
        Me.HorseRacingTipsIcon.Icon = CType(resources.GetObject("HorseRacingTipsIcon.Icon"), System.Drawing.Icon)
        Me.HorseRacingTipsIcon.Text = "競馬予想"
        Me.HorseRacingTipsIcon.Visible = True
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.終了ToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(99, 26)
        '
        '終了ToolStripMenuItem
        '
        Me.終了ToolStripMenuItem.Name = "終了ToolStripMenuItem"
        Me.終了ToolStripMenuItem.Size = New System.Drawing.Size(98, 22)
        Me.終了ToolStripMenuItem.Text = "終了"
        '
        'JVDataGetStop
        '
        Me.JVDataGetStop.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.JVDataGetStop.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.JVDataGetStop.Font = New System.Drawing.Font("MS UI Gothic", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.JVDataGetStop.Location = New System.Drawing.Point(297, 12)
        Me.JVDataGetStop.Name = "JVDataGetStop"
        Me.JVDataGetStop.Size = New System.Drawing.Size(249, 130)
        Me.JVDataGetStop.TabIndex = 1
        Me.JVDataGetStop.Text = "取得中止"
        Me.JVDataGetStop.UseVisualStyleBackColor = False
        '
        'GetTimer
        '
        Me.GetTimer.Enabled = True
        Me.GetTimer.Interval = 3600
        '
        'btnDBSetting
        '
        Me.btnDBSetting.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.btnDBSetting.Font = New System.Drawing.Font("MS UI Gothic", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.btnDBSetting.Location = New System.Drawing.Point(736, 12)
        Me.btnDBSetting.Name = "btnDBSetting"
        Me.btnDBSetting.Size = New System.Drawing.Size(166, 72)
        Me.btnDBSetting.TabIndex = 3
        Me.btnDBSetting.Text = "データベース設定"
        Me.btnDBSetting.UseVisualStyleBackColor = False
        '
        'lblCountDown
        '
        Me.lblCountDown.AutoSize = True
        Me.lblCountDown.Font = New System.Drawing.Font("MS UI Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.lblCountDown.Location = New System.Drawing.Point(215, 155)
        Me.lblCountDown.Name = "lblCountDown"
        Me.lblCountDown.Size = New System.Drawing.Size(0, 16)
        Me.lblCountDown.TabIndex = 9
        '
        'JVDataGetMst
        '
        Me.JVDataGetMst.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.JVDataGetMst.Font = New System.Drawing.Font("MS UI Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.JVDataGetMst.Location = New System.Drawing.Point(564, 90)
        Me.JVDataGetMst.Name = "JVDataGetMst"
        Me.JVDataGetMst.Size = New System.Drawing.Size(250, 46)
        Me.JVDataGetMst.TabIndex = 4
        Me.JVDataGetMst.Text = "年間開催スケジュール取得"
        Me.JVDataGetMst.UseVisualStyleBackColor = False
        '
        'lblVersion
        '
        Me.lblVersion.AutoSize = True
        Me.lblVersion.Location = New System.Drawing.Point(818, 170)
        Me.lblVersion.Name = "lblVersion"
        Me.lblVersion.Size = New System.Drawing.Size(42, 12)
        Me.lblVersion.TabIndex = 11
        Me.lblVersion.Text = "version"
        Me.lblVersion.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'JVDataGetPast
        '
        Me.JVDataGetPast.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.JVDataGetPast.Font = New System.Drawing.Font("MS UI Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(128, Byte))
        Me.JVDataGetPast.Location = New System.Drawing.Point(565, 142)
        Me.JVDataGetPast.Name = "JVDataGetPast"
        Me.JVDataGetPast.Size = New System.Drawing.Size(247, 40)
        Me.JVDataGetPast.TabIndex = 12
        Me.JVDataGetPast.Text = "過去データ取得"
        Me.JVDataGetPast.UseVisualStyleBackColor = False
        '
        'HorseRacingTips
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(922, 213)
        Me.Controls.Add(Me.JVDataGetPast)
        Me.Controls.Add(Me.lblVersion)
        Me.Controls.Add(Me.JVDataGetMst)
        Me.Controls.Add(Me.lblCountDown)
        Me.Controls.Add(Me.btnDBSetting)
        Me.Controls.Add(Me.JVDataGetStop)
        Me.Controls.Add(Me.ProgramEnd)
        Me.Controls.Add(Me.JVDataGetStart)
        Me.Controls.Add(Me.StatusStrip)
        Me.Controls.Add(Me.AxJVLink1)
        Me.Controls.Add(Me.btnJVInitSetting)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "HorseRacingTips"
        Me.Text = "競馬データ"
        CType(Me.AxJVLink1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip.ResumeLayout(False)
        Me.StatusStrip.PerformLayout()
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnJVInitSetting As Button
    Friend WithEvents AxJVLink1 As AxJVDTLabLib.AxJVLink
    Friend WithEvents StatusStrip As StatusStrip
    Friend WithEvents StatusLabel As ToolStripStatusLabel
    Friend WithEvents JVDataGetStart As Button
    Friend WithEvents ProgramEnd As Button
    Friend WithEvents HorseRacingTipsIcon As NotifyIcon
    Friend WithEvents ContextMenuStrip1 As ContextMenuStrip
    Friend WithEvents 終了ToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents JVDataGetStop As Button
    Friend WithEvents GetTimer As Timer
    Friend WithEvents btnDBSetting As Button
    Friend WithEvents lblCountDown As Label
    Friend WithEvents JVDataGetMst As Button
    Friend WithEvents lblVersion As Label
    Friend WithEvents JVDataGetPast As Button
End Class
